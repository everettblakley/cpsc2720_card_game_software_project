# Text-Based Card Games
*Course Project for CPSC2720, Fall 2018*
*Written by Everett Blakley and Logan Wilkie*

**Description**: This project contains the implementation of 3 text-based card games that run in a Linux terminal window. The card games are:
*  Go Fish
*  Crazy 8's
*  Exploding Kittens

**System Requirements**
*  Linux (*code is not designed for Windows execution*)
*  C++, with C++ 98 compiler

**To Play**
1.  Clone/Download the code for the repository
2.  In the terminal, run `make game`
3.  Select game to play
4.  Follow on-screen prompts