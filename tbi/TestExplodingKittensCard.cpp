/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "ExplodingKittensCard.h"
#include "Kind.h"
#include "gtest/gtest.h"

TEST(TestExplodingKittensCard, TestSameKind) {
  ExplodingKittensCard skip1(Kind::skip);
  ExplodingKittensCard skip2(Kind::skip);
  ExplodingKittensCard seeFuture1(Kind::see_future);
  ExplodingKittensCard explodingKitten1(Kind::exploding_kitten);
  ExplodingKittensCard shuffle1(Kind::shuffle);
  ExplodingKittensCard attack1(Kind::attack);
  ExplodingKittensCard attack2(Kind::attack);
  ExplodingKittensCard defuse1(Kind::defuse);

  /** Test for cards with the same kind
  */
  EXPECT_TRUE(skip1.sameKind(skip2));
  EXPECT_TRUE(skip2.sameKind(skip1));
  EXPECT_TRUE(attack1.sameKind(attack1));
  EXPECT_TRUE(attack2.sameKind(attack2));

  /** Test for cards with different ranks
  */
  EXPECT_FALSE(skip1.sameKind(seeFuture1));
  EXPECT_FALSE(defuse1.sameKind(explodingKitten1));
  EXPECT_FALSE(attack1.sameKind(skip2));
  EXPECT_FALSE(shuffle1.sameKind(defuse1));
  EXPECT_FALSE(explodingKitten1.sameKind(seeFuture1));
  EXPECT_FALSE(seeFuture1.sameKind(attack2));
}

TEST(TestExplodingKittensCard, TestGetKind) {
  ExplodingKittensCard favor1(Kind::favor);
  ExplodingKittensCard favor2(Kind::favor);
  ExplodingKittensCard nope1(Kind::nope);
  ExplodingKittensCard cat1(Kind::cat);
  ExplodingKittensCard shuffle1(Kind::shuffle);
  ExplodingKittensCard shuffle2(Kind::shuffle);
  ExplodingKittensCard defuse1(Kind::defuse);
  ExplodingKittensCard cat2(Kind::cat);

  /** Test for correct type
  */
  EXPECT_EQ(Kind::cat, cat2.getKind());
  EXPECT_EQ(Kind::nope, nope1.getKind());
  EXPECT_EQ(Kind::favor, favor2.getKind());
  EXPECT_EQ(Kind::defuse, defuse1.getKind());
  EXPECT_EQ(Kind::shuffle, shuffle2.getKind());

  /** Test for incorrect/non-matching types
  */
  EXPECT_FALSE(Kind::exploding_kitten == shuffle1.getKind());
  EXPECT_FALSE(Kind::see_future == cat1.getKind());
  EXPECT_FALSE(Kind::attack == favor1.getKind());
  EXPECT_FALSE(Kind::skip == nope1.getKind());
}

TEST(TestExplodingKittensCard, DISABLED_TestShow) {
    FAIL();
}
