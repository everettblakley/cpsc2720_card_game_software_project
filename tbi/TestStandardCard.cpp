/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "StandardCard.h"
#include "gtest/gtest.h"

TEST(TestStandardCard, TestConstructor) {
  /** Test if constructor throws proper exceptions for rank errors
   */
  EXPECT_THROW(StandardCard(Suit::clubs, 0), invalid_rank_error);
  EXPECT_THROW(StandardCard(Suit::hearts, 14), invalid_rank_error);
}

TEST(TestStandardCard, TestSameSuit) {
  StandardCard clubs1(Suit::clubs, 1);
  StandardCard clubs2(Suit::clubs, 1);
  StandardCard spades1(Suit::spades, 1);
  StandardCard spades2(Suit::spades, 1);
  StandardCard diamonds1(Suit::diamonds, 1);
  StandardCard diamonds2(Suit::diamonds, 1);
  StandardCard hearts1(Suit::hearts, 1);
  StandardCard hearts2(Suit::hearts, 1);

  /** Test for the same suit on objects with the same suit
  */
  EXPECT_TRUE(clubs1.sameSuit(clubs2));
  EXPECT_TRUE(spades1.sameSuit(spades2));
  EXPECT_TRUE(diamonds1.sameSuit(diamonds2));
  EXPECT_TRUE(hearts1.sameSuit(hearts2));

  /** Test for same suit on objects with different suits
  */
  EXPECT_FALSE(clubs1.sameSuit(diamonds1));
  EXPECT_FALSE(clubs1.sameSuit(hearts1));
  EXPECT_FALSE(clubs1.sameSuit(spades1));
  EXPECT_FALSE(diamonds1.sameSuit(hearts1));
  EXPECT_FALSE(diamonds1.sameSuit(spades1));
  EXPECT_FALSE(hearts1.sameSuit(spades1));

}

TEST(TestStandardCard, TestGetSuit) {
  StandardCard clubsCard(Suit::clubs, 1);
  StandardCard diamondsCard(Suit::diamonds, 1);
  StandardCard heartsCard(Suit::hearts, 1);
  StandardCard spadesCard(Suit::spades, 1);

  /** Test for the correct suit
  */
  EXPECT_EQ(Suit::clubs, clubsCard.getSuit());
  EXPECT_EQ(Suit::diamonds, diamondsCard.getSuit());
  EXPECT_EQ(Suit::hearts, heartsCard.getSuit());
  EXPECT_EQ(Suit::spades, spadesCard.getSuit());

  /** Test for the incorrect suit; may seems like overkill, but with 
   * only 4 types, may as well test them all
 */
  EXPECT_FALSE(Suit::clubs == diamondsCard.getSuit());
  EXPECT_FALSE(Suit::clubs == heartsCard.getSuit());
  EXPECT_FALSE(Suit::clubs == spadesCard.getSuit());
  EXPECT_FALSE(Suit::diamonds == clubsCard.getSuit());
  EXPECT_FALSE(Suit::diamonds == heartsCard.getSuit());
  EXPECT_FALSE(Suit::diamonds == spadesCard.getSuit());
  EXPECT_FALSE(Suit::hearts == clubsCard.getSuit());
  EXPECT_FALSE(Suit::hearts == diamondsCard.getSuit());
  EXPECT_FALSE(Suit::hearts == spadesCard.getSuit());
  EXPECT_FALSE(Suit::spades == clubsCard.getSuit());
  EXPECT_FALSE(Suit::spades == diamondsCard.getSuit());
  EXPECT_FALSE(Suit::spades == heartsCard.getSuit());
}

TEST(TestStandardCard, DISABLED_TestShow) {
  FAIL();
}


TEST(TestStandardStandardCard, TestSameRank) {
  StandardCard ace1(Suit::clubs, 1);
  StandardCard ace2(Suit::diamonds, 1);
  StandardCard jack(Suit::spades, 11);

  /** Test two aces
   */
  EXPECT_EQ(true, ace1.sameRank(ace2));

  /** Test two different cards
   */
  EXPECT_EQ(false, ace1.sameRank(jack));
}

TEST(TestStandardStandardCard, TestGetRank) {
  /** Loop through all possible rank values, check if the 
   * StandardCard object correctly returns that rank
   */
  for (int i = 1; i < 13; i++) {
    StandardCard card(Suit::clubs, i);
    EXPECT_EQ(i, card.getRank());
  }
}
