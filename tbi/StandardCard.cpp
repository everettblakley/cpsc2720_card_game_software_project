/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "StandardCard.h"
#include <iostream>
#include <string>

/**
* @return The Suit of the Card
*/
Suit StandardCard::getSuit() const {
  return suit;
}

/**
* @return The rank of the Card
*/
unsigned int StandardCard::getRank() const {
  return rank;
}

/**
* Check two Card objects for the same Rank
* @return True if the cards have the same Rank, false otherwise
*/
bool StandardCard::sameRank(const Card & otherCard) {
  return (this->rank == otherCard.getRank());
}

/**
* Check two Card objects for the same Suit
* @return True if the cards have the same Suit, false otherwise
*/
bool StandardCard::sameSuit(const Card & otherCard) {
    return (this->suit == otherCard.getSuit());
}

/**
* Output a StandardCard to the screen
*/
std::ostream& StandardCard::show() {
}
