/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "ExplodingKittensCard.h"
#include <iostream>
/**
* @return The Kind of the Card
*/
Kind ExplodingKittensCard::getKind() const {
  return kind;
}

/**
* Tests two Card objects for the same Kind
* @param otherCard The other Card to be examined
*/
bool ExplodingKittensCard::sameKind(const Card& otherCard) {
    return (this->kind == otherCard.getKind());
}

/**
* Output an ExplodingKittenCard to the screen
*/
std::ostream& ExplodingKittensCard::show() {
}
