/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef EXPLODINGKITTENSCARD_H
#define EXPLODINGKITTENSCARD_H

#include "Exceptions.h"
#include "Card.h"
#include "Kind.h"
#include "Suit.h"
#include <iostream>
#include <string>

class ExplodingKittensCard : public Card {
 public:
    /**
    * Constructor for an ExplodingKittensCard
    * @param k The Kind of the card
    */
    ExplodingKittensCard(const Kind& k)
     : Card(k, 1, Suit::exploding_kitten) {}
    virtual ~ExplodingKittensCard() {}

    /**
    * @return The Kind of the Card
    */
    virtual Kind getKind() const;

    /**
    * Method does nothing
    */
    virtual unsigned int getRank() const {}

    /**
    * Method does nothing
    */
    virtual Suit getSuit() const {}

    /**
    * Tests two Card objects for the same Kind
    * @param otherCard The other Card to be examined
    */
    virtual bool sameKind(const Card & otherCard);

    /**
    * Method does nothing
    */
    virtual bool sameRank(const Card & otherCard) {}

    /**
    * Method does nothing
    */
    virtual bool sameSuit(const Card & otherCard) {}

    /**
    * Output a card to the screen
    */
    std::ostream& show();
};

#endif
