/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef STANDARDCARD_H
#define STANDARDCARD_H

#include "Exceptions.h"
#include "Card.h"
#include "Kind.h"
#include "Suit.h"
#include <iostream>

class StandardCard : public Card {
 public:
     /**
     * Constructor for a StandardCard
     * @param s The Suit of the Card
     * @param r The rank of the Card
     * @throw illegal_rank_error Thrown if the rank is not between 1-13
     */
    StandardCard(const Suit& s, const unsigned int r)
      : Card(Kind::standard_card, r, s) {}

    virtual ~StandardCard() {}

    /**
    * Method does nothing
    */
    virtual Kind getKind() const {}

    /**
    * @return The rank of the Card as an integer
    */
    virtual unsigned int getRank() const;

    /**
    * @return The rank of the Card as an integer
    */
    virtual Suit getSuit() const;

    /**
    * Method does nothing
    */
    virtual bool sameKind(const Card & otherCard) {}

    /**
    * Tests two Card objects for the same rank
    * @param otherCard The other Card to be examined
    */
    virtual bool sameRank(const Card & otherCard);

    /**
    * Tests two Card objects for the same Suit
    * @param otherCard The other Card to be examined
    */
    virtual bool sameSuit(const Card & otherCard);

    /**
    * Output a card to the screen
    */
    virtual std::ostream& show();
};

#endif //STANDARDCARD_H
