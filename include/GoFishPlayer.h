/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef GOFISHPLAYER_H
#define GODISHPLAYER_H

#include "Player.h"
#include "Card.h"
#include <iostream>
#include <vector>

class GoFishPlayer : public Player {
 public:
    /**
    * Constructor for a GoFishPlayer
    * @param isComp Boolean variable determining the type of Player
    */
    GoFishPlayer(const bool isComp = false) : Player(isComp) {}

    /**
    * Destructor for a GoFishPlayer
    */
    ~GoFishPlayer();

    /**
    * "Fish" a opposing Player for a Card
    * @param opp The opponent to fish from
    * @param inCard The Card that will be compared to
    * @return True if the Card is fished successfully, false otherwise
    */
    bool fish(GoFishPlayer* opp, Card* inCard);

    /**
    * Determine the score of the GoFishPlayer by number of books
    * @return The score of the GoFishPlayer
    */
    int getScore();

    /**
    * Draws a Card from a pile and checks to see if a book was made
    * @param pile The CardPile to draw from.
    */
    void drawCard(CardPile* pile);

    /**
    * @return A pointer to the players books (vector of Cards)
    */
    std::vector<Card*>* getBooks();

    /**
    * Clears a players books. Deallocates and decreases size
    */
    void clearBooks();

 protected:
    std::vector<Card*> books;
};

#endif // GOFISHPLAYER_H
