/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef CARD_H
#define CARD_H

#include "Exceptions.h"
#include "Suit.h"
#include "Kind.h"
#include <iostream>

class Card {
 public:
    /**
    * Constructor for an ExplodingKittens Card
    * @param k The Kind of the Card
    */
    Card(const Kind& k);

    /**
    * @param s The Suit of the Card
    * @param r The integer Rank of the Card
    */
    Card(const Suit& s, const int& r);
    virtual ~Card() {}

    /**
    * @return The Kind of the Card
    */
    virtual Kind getKind() const;

    /**
    * @return The rank of the Card as an integer
    */
    virtual unsigned int getRank() const;

    /**
    * @return The Suit of the Card
    */
    virtual Suit getSuit() const;

    /**
    * Tests two Card objects for the same Kind
    * @param otherCard The other Card to be examined
    */
    virtual bool sameKind(const Card& otherCard);

    /**
    * Tests two Card objects for the same rank
    * @param otherCard The other Card to be examined
    */
    virtual bool sameRank(const Card& otherCard);

    /**
    * Tests two Card objects for the same Suit
    * @param otherCard The other Card to be examined
    */
    virtual bool sameSuit(const Card& otherCard);

    /**
    * Overloaded operator, Tests if two Card objects share all attributes
    * @param otherCard The other Card to be examined
    */
    bool operator==(const Card& otherCard);

    /**
    * Overloaded ostream output operator
    * @return The ostream representation of the Card
    */
    friend std::ostream& operator<<(std::ostream& os, const Card& c);

 protected:
    const Kind kind;
    unsigned int rank;
    const Suit suit;
};

#endif // CARD_H
