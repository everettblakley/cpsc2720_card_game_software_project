/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef CARDCONTAINER_H
#define CARDCONTAINER_H

#include "Exceptions.h"
#include "Card.h"
#include <iostream>
#include <vector>

class CardContainer {
 public:
    CardContainer() {}
    ~CardContainer() {}

    /**
    * @return The size of the CardContainer
    */
    int getSize();

    /**
    * Inserts a Card into a CardContainer, with an option for a specific index
    * @param inCard The card to be inserted
    * @param i The index to be inserted at
    * @throw illegal_card_container_error Thrown if the index is out of bounds
    */
    virtual void insertCard(Card* inCard, int i = -1);

    /**
    * "Pop" a Card off the top of the deck
    * @return The Card that was removed
    * @throw illegal_card_container_error Thrown if the CardContainer is empty
    */
    Card* pop();

    /**
    * @return The vector of Card pointers
    */
    std::vector<Card*> getCards();

 protected:
    std::vector<Card*> cards;
};

#endif  //CARDCONTAINER_H
