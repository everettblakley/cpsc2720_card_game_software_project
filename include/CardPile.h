/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef CARDPILE_H
#define CARDPILE_H

#include "CardContainer.h"
#include "Exceptions.h"
#include <iostream>

class CardPile : public CardContainer {
 public:
    /**
    * Constructor for a CardPile
    * @param maxS The maximum size of the CardPile (ie. 52 in Go Fish)
    */
    CardPile(unsigned int maxS) : maxSize(maxS) {}

    /**
    * Destructor for a CardPile, deallocates the vector of Card pointers
    */
    ~CardPile();

    /**
    * Shuffle a CardPile randomly
    */
    void shuffle();

    /**
    * @return The maximum size of the CardPile
    */
    int getMaxSize();

    /**
    * Insert a Card into the CardPile, either at a specified point or at the top
    * @param inCard The Card to be inserted
    * @param i The index to place the Card at, defaulted to -1 if the Card is to be
    * inserted at the back.
    */
    void insertCard(Card* inCard, int i = -1);

    /**
    * @return The top card of the CardPile, but does not remove
    */
    Card* top();

 protected:
    const unsigned int maxSize;
};

#endif //CARDPILE_H
