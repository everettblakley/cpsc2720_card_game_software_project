/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "Exceptions.h"
#include "CardPile.h"
#include "Card.h"
#include "Hand.h"


class Player {
 public:
    /**
    * Constructor for a Player
    * @param comp Boolean variable to determine if the Player is a computer
    */
    Player(const bool comp = false);

    /**
    * Destructor for a Player
    */
    virtual ~Player();

    /**
    * Draws a card from the top of the CardPile
    * @param pile The CardPile to be drawn from
    */
    virtual void drawCard(CardPile* pile);

    /**
    * Returns the Player objects Hand
    */
    Hand* getHand();

    /**
    * Returns the size of the Hand container object
    */
    int getHandSize();

    const bool isComputer;

 protected:
    Hand* hand;
};

#endif // PLAYER_H
