/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Logan Wilkie <logan.wilkie@uleth.ca>
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef SUIT_H
#define SUIT_H

#include "Exceptions.h"
#include <iostream>

enum class Suit {clubs = 0,
                diamonds = 1,
                hearts = 2,
                spades = 3, exploding_kitten};
/**
* Overloaded << operator, to allow output of the Suit
* @return output stream used
*/
std::ostream& operator<< (std::ostream& os, const Suit& suit);

#endif //SUIT_H
