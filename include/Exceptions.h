/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef EXCEPTIONS_H_INCLUDED
#define EXCEPTIONS_H_INCLUDED

#include <stdexcept>

/**
 * Exception for when a rank is not within the allowed range of rank values
 */
class invalid_rank_error : public std::runtime_error {
 public:
    /** @param errMessage An error message
    */
    explicit invalid_rank_error(const char* errMessage) :
      std::runtime_error(errMessage) {
      }
};

/**
* Exception for when an illegal action is performed on a CardContainer
*/
class illegal_card_container_error : public std::runtime_error {
 public:
    explicit illegal_card_container_error(const char* errMessage) :
      std::runtime_error(errMessage) {
      }
};

/**
* Exception for when an illegal action is performed with a Player
*/
class illegal_player_operation : public std::runtime_error {
 public:
    explicit illegal_player_operation(const char* errMessage) :
      std::runtime_error(errMessage) {}
};


#endif
