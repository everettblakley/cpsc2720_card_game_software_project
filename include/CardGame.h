/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef CARDGAME_H
#define CARDGAME_H

#include "Player.h"
#include "CardPile.h"
#include <iostream>
#include <string>

class CardGame {
 public:
    CardGame() {}
    ~CardGame() {}

    /**
    * Displays the main menu for a game
    */
    virtual void mainMenu() = 0;

    /**
    * Display text from a file, whether it be game rules or a user manual
    * @param filePath The path to take to the file containing the text
    */
    void displayText(std::string filePath);

 protected:
    /**
    * Begins the turn rotation sequence, and checks for the end of the game
    */
    virtual void playGame() = 0;

    /**
    * Saves a game in it's current state
    */
    virtual void saveGame() = 0;

    /**
    * Loads a game state from a file
    */
    virtual void loadGame() = 0;

    /**
    * Starts a game from scratch
    */
    void newGame();

    /**
    * Checks to see if the game has been completed
    */
    virtual void checkGameOver() = 0;

    /**
    * Displays a menu for player actions (within a game)
    */
    virtual void menu() = 0;

    /**
    * Initialize the deck, shuffle, and deal to players
    */
    virtual void deal() = 0;

    /**
    * Reset the parameters of the CardGame
    */
    virtual void resetGame() = 0;

    CardPile* drawPile;
    std::string rulesFilePath;
    std::string saveFilePath;
    int turnCount = 0;
    bool gameOver = false;
};

#endif // CARDGAME_H
