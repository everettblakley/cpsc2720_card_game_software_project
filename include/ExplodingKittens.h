/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef EXPLODINGKITTENS_H
#define EXPLODINGKITTENS_H

#include "CardGame.h"
#include "ExplodingKittensPlayer.h"
#include <vector>
#include <iostream>
#include <string>

class ExplodingKittens : public CardGame {
 public:
    /**
    * Constructor for an Exploding Kittens game. Sets file paths and
    * intializes players
    */
    ExplodingKittens();

    /**
    * Destructor for an Exploding Kitten
    */
    ~ExplodingKittens();

    /**
    * Displays the main menu for an Exploding Kittens game, allowing saving,
    * loading, seeing the rules of the game and quitting
    */
    virtual void mainMenu();

 protected:
    /**
    * Starts the game/turn rotations, and checks for if the game is over
    */
    virtual void playGame();

    /**
    * Saves the game in its current state to the save file
    */
    virtual void saveGame();

    /**
    * Loads a game state from a save file
    */
    virtual void loadGame();

    /**
    * Checks the game over conditions of the game
    */
    virtual void checkGameOver();

    /**
    * Displays an in-game menu with a list of player actions
    */
    virtual void menu();

    /**
    * Deals cards to each of the players, then gives them each defuse cards and
    * places the exploding_kitten randomly within the drawPile
    */
    virtual void deal();

    /**
    * Reset all the values of the game back to the original state, used if the
    * user decides to quit at any point
    */
    virtual void resetGame();

    /**
    * The player turn rotation for a player. Plays the computer turn for them
    * @param player The player whos turn it is
    */
    void playerTurn(ExplodingKittensPlayer* player);

    /**
    * The action function for a human player, allows them to draw or play a card
    */
    void playing(bool* pass);

    /**
    * The procedure for when a Kitten is picked up and a defuse is avaliable
    * @param player The player who will defuse the kitten and relocate the
    * Exploding Kitten.
    * @param dIndex The index of the defuse to be removed, used to save time
    */
    void kittenProcedure(ExplodingKittensPlayer* player, int dIndex);

    /**
    * Function for when a player is able to "nope" another player's play
    * @param p The player who might "nope" a play
    */
    bool nope(ExplodingKittensPlayer* p);

    /**
    * Perform an action form one of various action cards
    * @param p The Player performing the action
    * @param c The Card to be played
    */
    bool performCardAction(ExplodingKittensPlayer* p, ExplodingKittensPlayer* o,
      Card* c);

    /**
    * Prompts the player for a card to play (when they want to play a card)
    * @param cp The CardPile to check the validity of a play
    */
    int promptForPlay(CardPile* cp);

    std::vector<ExplodingKittensPlayer*> players;
    CardPile* discardPile;
};

#endif
