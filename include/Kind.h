/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef KIND_H
#define KIND_H

#include "Exceptions.h"
#include <iostream>

enum class Kind {exploding_kitten, defuse, nope, attack, skip,
                favor, shuffle, see_future, cat, standard_card};
/**
* Overloaded << operator, to allow output of the Kind
* @return output stream used
*/
std::ostream& operator<< (std::ostream& os, const Kind& kind);

#endif //KIND_H
