/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef EXPLODINGKITTENSPLAYER_H
#define EXPLODINGKITTENSPLAYER_H

#include "ActionBasedPlayer.h"
#include "Card.h"

class ExplodingKittensPlayer : public ActionBasedPlayer {
 public:
    /**
    * Constructor for a ExplodingKittensPlayer
    * @param isComp Boolean variable indicating a computer or a human
    */
    ExplodingKittensPlayer(const bool isComp = false)
     : ActionBasedPlayer(isComp) {}

    /**
    * Destructor for a CrazyEightsPlayer
    */
    virtual ~ExplodingKittensPlayer();

    /**
    * Check to see if a certain Card is a valid play
    * @param card The Card to be examined
    * @param pile The CardPile to be compared to (top Card)
    * @return True if the play is valid, false otherwise
    */
    virtual bool validPlay(const Card* card, CardPile* pile);

    /**
    * Draws a card from a CardPile
    * @param pile The CardPile to draw from
    */
    void drawCard(CardPile* pile);
};

#endif //EXPLODINGKITTENSPLAYER_H
