/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef HAND_H
#define HAND_H

#include "CardContainer.h"
#include "Exceptions.h"
#include <iostream>
#include <vector>

class Hand : public CardContainer {
 public:
    /**
    * Constructor for a Hand
    * @param intitialSize The initial size of the Hand vector
    */
    Hand() {}

    /**
    * Destructor for a Hand object, deallocates the vector of Card pointers
    */
    ~Hand();

    /**
    * Allows output of a Hand to the screen
    */
    friend std::ostream& operator<<(std::ostream& os, Hand& h);

    /**
     * Removes Card at index i
     * @param i index of the card to be removed
     * @throw illegal_card_container_error if \a i is outside bounds
     */
    Card* removeCard(int i);

    /**
     * Finds the index of a Card in a Hand
     * @param inCard a pointer to the Card in question
     * @return the index if the it is in the hands, -1 if it is not
     */
    int find(Card* inCard);
};

#endif // CARD_H
