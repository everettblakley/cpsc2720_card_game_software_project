/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef GOFISH_H
#define GOFISH_H

#include "CardGame.h"
#include "GoFishPlayer.h"
#include "Card.h"
#include <vector>
#include <iostream>
#include <string>

class GoFish : public CardGame {
 public:
    /**
    * Constructor for a GoFish game
    */
    GoFish();

    /**
    * Destructor for a GoFish game
    */
    ~GoFish();

    /**
    * Displays the main menu for a GoFish game
    */
    virtual void mainMenu();

 protected:
    /**
    * Begins the game process after creating or loading a game
    */
    virtual void playGame();

    /**
    * Saves a game in it's current state, storing players' hands, books
    * and the deck of cards
    */
    virtual void saveGame();

    /**
    * Loads a game state from a file and begins turn rotation
    */
    virtual void loadGame();

    /**
    * Checks to see if the game has been completed
    */
    virtual void checkGameOver();

    /**
    * Displays a menu for a GoFish player, allowing them to perform
    * various actions
    */
    virtual void menu();

    /**
    * Deals card to each player after initializing the draw pile
    * Checks for immediate books that may have formed
    */
    virtual void deal();

    /**
    * Reset the parameters of the CardGame
    */
    virtual void resetGame();

    /**
    * Begins a turn sequence for an individual player, or a pre-made turn
    * sequence for a computer opponent
    * @param player The current player
    * @param opp That player's opponent
    */
    void playerTurn(GoFishPlayer* player, GoFishPlayer* opp);

    /**
    * Displays a scoreboard showing number of books for each player
    */
    void scoreboard();

    /**
    * To go fishing for a card within the confines of a GoFish game
    * @param wentFishing True if the player
    */
    void fishing(bool* wentFishing);

    /**
    * Checks a players hand for certain conditions
    */
    void checkHand(GoFishPlayer* player);

    std::vector<GoFishPlayer*> players;
};

#endif // GOFISH_H
