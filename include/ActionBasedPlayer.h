/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef ACTIONBASEDPLAYER_H
#define ACTIONBASEDPLAYER_H

#include "Player.h"
#include "CardPile.h"
#include "Card.h"

class ActionBasedPlayer : public Player {
 public:
    /**
    * Constructor for an ActionBasedPlayer
    * @param isComp Boolean variable to determine the sentience of the Player
    */
    ActionBasedPlayer(const bool isComp = false) : Player(isComp) {}

    virtual ~ActionBasedPlayer() {}

    /**
    * Plays a Card to the CardPile
    * @param card The Card to be played, if valid
    * @param pile The CardPile the Card is played to
    */
    void playCard(unsigned int index, CardPile* pile);

    /**
    * Check to see if a certain Card is a valid play
    * Implemented in subclasses
    */
    virtual bool validPlay(const Card* card, CardPile* pile) = 0;

    /**
    * Check a Player objects Hand to see if they are able to make any plays
    * @param cp The CardPile that the Card is being tested validity against
    * @return True if there is at least 1 valid play, false otherwise
    */
    bool anyValidPlays(CardPile* cp);
};

#endif //ACTIONBASEDPLAYER_H
