/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef CRAZYEIGHTS_H
#define CRAZYEIGHTS_H

#include "CardGame.h"
#include "CrazyEightsPlayer.h"
#include "Card.h"
#include <vector>
#include <iostream>
#include <string>
#include "Suit.h"

class CrazyEights : public CardGame {
 public:
    /**
    * Constructor for a CrazyEights game. Initializes players and file paths
    */
    CrazyEights();

    /**
    * Destructor for a CrazyEights game
    */
    ~CrazyEights();

    /**
    * Displays a mainMenu for the game, allowing players to create, load,
    * get help and quit a game.
    */
    virtual void mainMenu();

 protected:
    /**
    * Begins the turn roation sequence, and checks for the end of each game
    */
    virtual void playGame();

    /**
    * Saves a game in its current state to a local text file
    */
    virtual void saveGame();

    /**
    * Loads a game state by coverting a text file to a game. Begins the game
    */
    virtual void loadGame();

    /**
    * Checks the game for a game over condition, and changes the boolean
    * variable accoridingly
    */
    virtual void checkGameOver();

    /**
    * Displays a menu in game, allowing players to play cards, save, view
    * scores, see the rules and quit.
    */
    virtual void menu();

    /**
    * Intializes the deck, and deals cards to each player
    */
    virtual void deal();

    /**
    * Resets the attributes of the game after quitting or beginning a new game.
    */
    virtual void resetGame();

    /**
    * Begins a turn sequence for an individual player, or a pre-made turn
    * sequence for a computer opponent
    * @param p The player who is taking their turn
    */
    void playerTurn(CrazyEightsPlayer* p);

    /**
    * View the scoreboard of the game, in this case the number of cards left
    * by each player
    */
    void scoreboard();

    /**
    * Functions used for when a human player is making their turn and decides to
    * play a card
    */
    void playing();

    /**
    * Checks if the draw pile is empty, and reinitializes should it be empty.
    */
    void pileEmpty();

    /**
    * Prompts a player for an index of a card in thier hand to play
    * @param correctPile The CardPile to check for validity of a play
    */
    int promptForPlay(CardPile* correctPile);

    /**
    * Checks to see if a player's lst play was an eight, and prompts them for a
    * new Suit if true
    * @param p The player to be prompted, if true.
    */
    void playedEight(CrazyEightsPlayer* p);

    std::vector<CrazyEightsPlayer*> players;
    CardPile* discardPile;
    Suit currentSuit;
};

#endif //CRAZYEIGHTS_H
