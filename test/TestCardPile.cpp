/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CardPile.h"
#include <iostream>
#include <vector>
#include "Card.h"
#include "gtest/gtest.h"
#include "Exceptions.h"

TEST(TestCardPile, TestSCShuffle) {
  CardPile cp(10);
  cp.shuffle(); //Test that you can still shuffle an empty CardPile

  Card* sc1 = new Card(Suit::hearts, 1);
  Card* sc2 = new Card(Suit::hearts, 10);
  Card* sc3 = new Card(Suit::diamonds, 2);
  Card* sc4 = new Card(Suit::diamonds, 11);
  Card* sc5 = new Card(Suit::spades, 3);
  Card* sc6 = new Card(Suit::spades, 12);
  Card* sc7 = new Card(Suit::clubs, 4);
  Card* sc8 = new Card(Suit::clubs, 13);
  cp.insertCard(sc1);
  cp.insertCard(sc2);
  cp.insertCard(sc3);
  cp.insertCard(sc4);
  cp.insertCard(sc5);
  cp.insertCard(sc6);
  cp.insertCard(sc7);
  cp.insertCard(sc8);

  std::vector<Card*> cardsBefore = cp.getCards();
  cp.shuffle();
  std::vector<Card*> cardsAfter = cp.getCards();

  bool cpChanged = false;
  for (int j = 0; j < cardsBefore.size(); j++) {
    std::cout << *cardsBefore[j] << "\t" << *cardsAfter[j] << std::endl;
    if (!(cardsBefore[j] == cardsAfter[j])) {
      cpChanged = true;
    }
  }
  EXPECT_TRUE(cpChanged);
}

TEST(TestCardPile, TestEKShuffle) {
  CardPile cp(11);

  Card* ek1 = new Card(Kind::exploding_kitten);
  Card* ek2 = new Card(Kind::defuse);
  Card* ek3 = new Card(Kind::nope);
  Card* ek4 = new Card(Kind::attack);
  Card* ek5 = new Card(Kind::skip);
  Card* ek6 = new Card(Kind::favor);
  Card* ek7 = new Card(Kind::shuffle);
  Card* ek8 = new Card(Kind::see_future);
  Card* ek9 = new Card(Kind::cat);
  cp.insertCard(ek1);
  cp.insertCard(ek2);
  cp.insertCard(ek3);
  cp.insertCard(ek4);
  cp.insertCard(ek5);
  cp.insertCard(ek6);
  cp.insertCard(ek7);
  cp.insertCard(ek8);
  cp.insertCard(ek9);

  std::vector<Card*> cardsBefore = cp.getCards();
  cp.shuffle();
  std::vector<Card*> cardsAfter = cp.getCards();

  bool cpChanged = false;
  for (int j = 0; j < cardsBefore.size(); j++) {
    std::cout << *cardsBefore[j] << "\t" << *cardsAfter[j] << std::endl;
    if (!(cardsBefore[j] == cardsAfter[j])) {
      cpChanged = true;
    }
  }
  EXPECT_TRUE(cpChanged);
}

TEST(TestCardPile, TestTop) {
  CardPile cp3(2);
  EXPECT_TRUE(cp3.getCards().empty());
  EXPECT_THROW(cp3.top(), illegal_card_container_error);

  Card* ekc1 = new Card(Kind::defuse);
  Card* ekc2 = new Card(Kind::attack);
  Card* ekc3 = new Card(Kind::skip);

  cp3.insertCard(ekc1);
  EXPECT_TRUE(ekc1 == cp3.top());

  cp3.insertCard(ekc2);
  EXPECT_FALSE(ekc1 == cp3.top());

  EXPECT_THROW(cp3.insertCard(ekc3), illegal_card_container_error);
  EXPECT_TRUE(ekc2 == cp3.top());

  delete ekc3;
  ekc3 = nullptr;
}
