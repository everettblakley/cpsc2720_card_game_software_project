/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "Hand.h"
#include <vector>
#include "gtest/gtest.h"
#include "Card.h"

TEST(TestHand, TestGetSize) {
  Hand hand1;

  EXPECT_EQ(0, hand1.getSize());

  for (int i = 1; i <= 5; i++) {
    Card* sc = new Card(Suit::diamonds, i);
    hand1.insertCard(sc);
  }

  EXPECT_EQ(5, hand1.getSize());

  Card* c = hand1.pop();

  EXPECT_EQ(4, hand1.getSize());

  delete c;
  c = nullptr;
}

TEST(TestHand, TestInsertCard) {
  Hand hand1;

  Card* ek1 = new Card(Kind::skip);
  hand1.insertCard(ek1);
  EXPECT_TRUE(hand1.getCards()[0] == ek1);
  Card* ek2 = new Card(Kind::defuse);
  hand1.insertCard(ek2);
  EXPECT_TRUE(hand1.getCards()[1] == ek2);
  Card* ek3 = new Card(Kind::nope);
  hand1.insertCard(ek3, 1);
  Card* ek4 = new Card(Kind::defuse);
  hand1.insertCard(ek4);
  EXPECT_TRUE(hand1.getCards()[1] == ek3);
  EXPECT_TRUE(hand1.getCards()[2] == ek2);

  EXPECT_THROW(hand1.insertCard(ek2, 10), illegal_card_container_error);
}

TEST(TestHand, TestGetCards) {
  Hand hand1;
  EXPECT_TRUE(hand1.getCards().empty());

  for (int i = 1; i < 5; i++) {
    Card* sc = new Card(Suit::spades, i);
    hand1.insertCard(sc);
  }

  std::vector<Card*> tempVec = hand1.getCards();

  for (int j = 0; j < hand1.getSize(); j++) {
    EXPECT_TRUE(tempVec[j] == hand1.getCards()[j]);
  }

  Card* c = hand1.pop();
  for (int k = 0; k < hand1.getSize(); k++) {
    EXPECT_FALSE(hand1.getCards()[k] == c);
  }
  delete c;
  c = nullptr;
}

TEST(TestHand, TestPop) {
  Hand hand1;

  Card* ek1 = new Card(Kind::skip);
  Card* ek2 = new Card(Kind::defuse);
  Card* ek3 = new Card(Kind::nope);
  hand1.insertCard(ek1);
  hand1.insertCard(ek2);
  hand1.insertCard(ek3);

  EXPECT_TRUE(ek3 == hand1.pop());
  EXPECT_TRUE(ek2 == hand1.pop());
  EXPECT_TRUE(ek1 == hand1.pop());

  EXPECT_TRUE(hand1.getCards().empty());

  delete ek1;
  ek1 = nullptr;
  delete ek2;
  ek2 = nullptr;
  delete ek3;
  ek3 = nullptr;
}

TEST(TestHand, TestRemoveCard) {
  Hand h;

  Card* c = new Card(Suit::diamonds, 5);
  EXPECT_THROW(h.removeCard(1), illegal_card_container_error);

  h.insertCard(c);
  EXPECT_EQ(c, h.removeCard(0));

  delete c;
  c = nullptr;
}

TEST(TestHand, TestFind) {
  Hand h;

  Card* c = new Card(Kind::defuse);
  EXPECT_EQ(-1, h.find(c));

  h.insertCard(c);
  EXPECT_EQ(0, h.find(c));
}
