/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "Player.h"
#include "Hand.h"
#include "CardPile.h"
#include "Card.h"
#include "gtest/gtest.h"

TEST(TestPlayer, TestDrawCard) {
  Player p1;
  CardPile cp(10);
  EXPECT_THROW(p1.drawCard(&cp), illegal_card_container_error);

  for (int i = 0; i < cp.getMaxSize(); i++) {
    Card* sc = new Card(Suit::clubs, i+1);
    cp.insertCard(sc);
  }

  Card* top = cp.top();
  p1.drawCard(&cp);
  EXPECT_TRUE(p1.getHand()->getCards()[p1.getHandSize() - 1] == top);
}

TEST(TestPlayer, TestGetHand) {
  Player p1;
  Hand* h = p1.getHand();
  EXPECT_TRUE(h->getCards().empty());

  CardPile cp(10);

  for (int i = 0; i < cp.getMaxSize(); i++) {
    Card* sc = new Card(Suit::clubs, i+1);
    cp.insertCard(sc);
  }

  cp.shuffle();
  Card* top = cp.top();
  p1.drawCard(&cp);
  EXPECT_TRUE(p1.getHand()->getCards()[p1.getHandSize() - 1] == top);
}
