/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CrazyEightsPlayer.h"
#include "gtest/gtest.h"

TEST(TestCrazyEightsPlayer, TestValidPlay) {
  CrazyEightsPlayer c8p1;
  CardPile discard(1);
  CardPile drawPile(5);

  Card* clubs1 = new Card(Suit::clubs, 13);
  discard.insertCard(clubs1);

  Card* clubs2 = new Card(Suit::clubs, 10);
  drawPile.insertCard(clubs2);
  Card* spades1 = new Card(Suit::spades, 8);
  drawPile.insertCard(spades1);
  Card* hearts1 = new Card(Suit::hearts, 13);
  drawPile.insertCard(hearts1);
  Card* diamonds1 = new Card(Suit::diamonds, 5);
  drawPile.insertCard(diamonds1);
  c8p1.drawCard(&drawPile);
  c8p1.drawCard(&drawPile);
  c8p1.drawCard(&drawPile);
  c8p1.drawCard(&drawPile);


  Card* ek1 = new Card(Suit::exploding_kitten, 1);
  c8p1.getHand()->insertCard(ek1);

  EXPECT_FALSE(c8p1.validPlay(c8p1.getHand()->getCards()[0], &discard));
  EXPECT_TRUE(c8p1.validPlay(c8p1.getHand()->getCards()[1], &discard));
  EXPECT_TRUE(c8p1.validPlay(c8p1.getHand()->getCards()[2], &discard));
  EXPECT_TRUE(c8p1.validPlay(c8p1.getHand()->getCards()[3], &discard));
  EXPECT_FALSE(c8p1.validPlay(c8p1.getHand()->getCards()[4], &discard));
}
