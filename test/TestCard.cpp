/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "Card.h"
#include "Exceptions.h"
#include "gtest/gtest.h"


TEST(TestCard, TestSameKind) {
  Card skip1(Kind::skip);
  Card skip2(Kind::skip);
  Card seeFuture1(Kind::see_future);
  Card explodingKitten1(Kind::exploding_kitten);
  Card shuffle1(Kind::shuffle);
  Card attack1(Kind::attack);
  Card attack2(Kind::attack);
  Card defuse1(Kind::defuse);

  /** Test for cards with the same kind
  */
  EXPECT_TRUE(skip1.sameKind(skip2));
  EXPECT_TRUE(skip2.sameKind(skip1));
  EXPECT_TRUE(attack1.sameKind(attack1));
  EXPECT_TRUE(attack2.sameKind(attack2));

  /** Test for cards with different ranks
  */
  EXPECT_FALSE(skip1.sameKind(seeFuture1));
  EXPECT_FALSE(defuse1.sameKind(explodingKitten1));
  EXPECT_FALSE(attack1.sameKind(skip2));
  EXPECT_FALSE(shuffle1.sameKind(defuse1));
  EXPECT_FALSE(explodingKitten1.sameKind(seeFuture1));
  EXPECT_FALSE(seeFuture1.sameKind(attack2));

  Card sc1(Suit::diamonds, 5);
  Card sc2(Suit::spades, 13);

  EXPECT_TRUE(sc1.sameKind(sc2));
}

TEST(TestCard, TestGetKind) {
  Card favor1(Kind::favor);
  Card favor2(Kind::favor);
  Card nope1(Kind::nope);
  Card cat1(Kind::cat);
  Card shuffle1(Kind::shuffle);
  Card shuffle2(Kind::shuffle);
  Card defuse1(Kind::defuse);
  Card cat2(Kind::cat);

  /** Test for correct type
  */
  EXPECT_EQ(Kind::cat, cat2.getKind());
  EXPECT_EQ(Kind::nope, nope1.getKind());
  EXPECT_EQ(Kind::favor, favor2.getKind());
  EXPECT_EQ(Kind::defuse, defuse1.getKind());
  EXPECT_EQ(Kind::shuffle, shuffle2.getKind());

  /** Test for incorrect/non-matching types
  */
  EXPECT_FALSE(Kind::exploding_kitten == shuffle1.getKind());
  EXPECT_FALSE(Kind::see_future == cat1.getKind());
  EXPECT_FALSE(Kind::attack == favor1.getKind());
  EXPECT_FALSE(Kind::skip == nope1.getKind());

  Card sc1(Suit::hearts, 1);

  EXPECT_TRUE(Kind::standard_card == sc1.getKind());
}

TEST(TestCard, TestConstructor) {
  /** Test if constructor throws proper exceptions for rank errors
   */
  EXPECT_THROW(Card(Suit::clubs, 0), invalid_rank_error);
  EXPECT_THROW(Card(Suit::hearts, 14), invalid_rank_error);
}

TEST(TestCard, TestSameSuit) {
  Card clubs1(Suit::clubs, 1);
  Card clubs2(Suit::clubs, 1);
  Card spades1(Suit::spades, 1);
  Card spades2(Suit::spades, 1);
  Card diamonds1(Suit::diamonds, 1);
  Card diamonds2(Suit::diamonds, 1);
  Card hearts1(Suit::hearts, 1);
  Card hearts2(Suit::hearts, 1);

  /** Test for the same suit on objects with the same suit
  */
  EXPECT_TRUE(clubs1.sameSuit(clubs2));
  EXPECT_TRUE(spades1.sameSuit(spades2));
  EXPECT_TRUE(diamonds1.sameSuit(diamonds2));
  EXPECT_TRUE(hearts1.sameSuit(hearts2));

  /** Test for same suit on objects with different suits
  */
  EXPECT_FALSE(clubs1.sameSuit(diamonds1));
  EXPECT_FALSE(clubs1.sameSuit(hearts1));
  EXPECT_FALSE(clubs1.sameSuit(spades1));
  EXPECT_FALSE(diamonds1.sameSuit(hearts1));
  EXPECT_FALSE(diamonds1.sameSuit(spades1));
  EXPECT_FALSE(hearts1.sameSuit(spades1));

  Card ek1(Kind::shuffle);
  Card ek2(Kind::favor);

  EXPECT_TRUE(ek1.sameSuit(ek2));
}

TEST(TestCard, TestGetSuit) {
  Card clubsCard(Suit::clubs, 1);
  Card diamondsCard(Suit::diamonds, 1);
  Card heartsCard(Suit::hearts, 1);
  Card spadesCard(Suit::spades, 1);

  /** Test for the correct suit
  */
  EXPECT_EQ(Suit::clubs, clubsCard.getSuit());
  EXPECT_EQ(Suit::diamonds, diamondsCard.getSuit());
  EXPECT_EQ(Suit::hearts, heartsCard.getSuit());
  EXPECT_EQ(Suit::spades, spadesCard.getSuit());

  /** Test for the incorrect suit; may seems like overkill, but with
   * only 4 types, may as well test them all
 */
  EXPECT_FALSE(Suit::clubs == diamondsCard.getSuit());
  EXPECT_FALSE(Suit::clubs == heartsCard.getSuit());
  EXPECT_FALSE(Suit::clubs == spadesCard.getSuit());
  EXPECT_FALSE(Suit::diamonds == clubsCard.getSuit());
  EXPECT_FALSE(Suit::diamonds == heartsCard.getSuit());
  EXPECT_FALSE(Suit::diamonds == spadesCard.getSuit());
  EXPECT_FALSE(Suit::hearts == clubsCard.getSuit());
  EXPECT_FALSE(Suit::hearts == diamondsCard.getSuit());
  EXPECT_FALSE(Suit::hearts == spadesCard.getSuit());
  EXPECT_FALSE(Suit::spades == clubsCard.getSuit());
  EXPECT_FALSE(Suit::spades == diamondsCard.getSuit());
  EXPECT_FALSE(Suit::spades == heartsCard.getSuit());

  Card ek1(Kind::nope);

  EXPECT_EQ(Suit::exploding_kitten, ek1.getSuit());
}

TEST(TestCard, TestSameRank) {
  Card ace1(Suit::clubs, 1);
  Card ace2(Suit::diamonds, 1);
  Card jack(Suit::spades, 11);

  /** Test two aces
   */
  EXPECT_EQ(true, ace1.sameRank(ace2));

  /** Test two different cards
   */
  EXPECT_EQ(false, ace1.sameRank(jack));

  Card ek1(Kind::cat);
  Card ek2(Kind::defuse);

  EXPECT_TRUE(ek1.sameRank(ek2));
}

TEST(TestCard, TestGetRank) {
  /** Loop through all possible rank values, check if the
   * Card object correctly returns that rank
   */
  for (int i = 1; i < 13; i++) {
    Card card(Suit::clubs, i);
    EXPECT_EQ(i, card.getRank());
  }
  Card ek1(Kind::see_future);

  EXPECT_EQ(1, ek1.getRank());
}

TEST(TestCard, TestEquality) {
  Card sc1(Suit::diamonds, 1);
  Card sc2(Suit::diamonds, 1);
  Card sc3(Suit::hearts, 2);

  EXPECT_TRUE(sc1 == sc2);
  EXPECT_FALSE(sc2 == sc3);
}
