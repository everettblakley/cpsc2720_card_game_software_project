/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "GoFishPlayer.h"
#include "CardPile.h"
#include "Card.h"
#include "gtest/gtest.h"

TEST(TestGoFishPlayer, TestFish) {
  GoFishPlayer p1, p2;

  CardPile cp(10);
  for (int i = 0; i < cp.getMaxSize()/2; i++) {
    Card* sc1 = new Card(Suit::diamonds, i+1);
    Card* sc2 = new Card(Suit::clubs, i+1);
    cp.insertCard(sc1);
    cp.insertCard(sc2);
    p1.drawCard(&cp);
    if (sc2->getRank() != 2) {
      p2.drawCard(&cp);
    }
  }

  EXPECT_TRUE(p1.fish(&p2, p1.getHand()->getCards()[0]));

  EXPECT_EQ(1, p1.getScore());

  EXPECT_FALSE(p1.fish(&p2, p1.getHand()->getCards()[0]));
}

TEST(TestGoFishPlayer, TestGetScore) {
  GoFishPlayer p1, p2(true);
  EXPECT_EQ(0, p1.getScore());

  CardPile cp(2);
  Card* sc1 = new Card(Suit::diamonds, 5);
  Card* sc2 = new Card(Suit::clubs, 5);
  cp.insertCard(sc1);
  cp.insertCard(sc2);

  p1.drawCard(&cp);
  p2.drawCard(&cp);

  p2.fish(&p1, p2.getHand()->getCards()[0]);
  EXPECT_EQ(1, p2.getScore());
  EXPECT_EQ(2, p2.getBooks()->size());

  p2.clearBooks();
  EXPECT_EQ(0, p2.getBooks()->size());
}

TEST(TestGoFish, TestSelfFish) {
  GoFishPlayer* p1 = new GoFishPlayer();
  CardPile* cp = new CardPile(10);

  for (int i = 1; i < 6; i++) {
    Card* c = new Card(Suit::diamonds, i);
    cp->insertCard(c);
    p1->drawCard(cp);
  }
  std::cout << *(p1->getHand()) << std::endl;
  cp->insertCard(new Card(Suit::hearts, 4));
  p1->drawCard(cp);
  std::cout << *(p1->getHand()) << std::endl;

  delete p1;
  p1 = nullptr;
  delete cp;
  cp = nullptr;
}
