/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "ExplodingKittensPlayer.h"
#include "Card.h"
#include "gtest/gtest.h"

TEST(TestExplodingKittensPlayer, TestValidPlay) {
  ExplodingKittensPlayer p1;
  CardPile discard(6);

  Card* cat1 = new Card(Kind::cat);
  Card* ek = new Card(Kind::exploding_kitten);
  Card* defuse1 = new Card(Kind::defuse);
  Card* shuffle1 = new Card(Kind::shuffle);
  Card* nope1 = new Card(Kind::nope);
  Card* nope2 = new Card(Kind::nope);

  p1.getHand()->insertCard(nope1);
  EXPECT_FALSE(p1.validPlay(p1.getHand()->getCards()[0], &discard));

  discard.insertCard(cat1);
  EXPECT_FALSE(p1.validPlay(p1.getHand()->getCards()[0], &discard));
  p1.drawCard(&discard);

  discard.insertCard(defuse1);
  EXPECT_FALSE(p1.validPlay(p1.getHand()->getCards()[0], &discard));
  p1.drawCard(&discard);

  discard.insertCard(shuffle1);
  EXPECT_TRUE(p1.validPlay(p1.getHand()->getCards()[0], &discard));
  p1.drawCard(&discard);

  discard.insertCard(nope2);
  EXPECT_TRUE(p1.validPlay(p1.getHand()->getCards()[0], &discard));

  EXPECT_FALSE(p1.validPlay(p1.getHand()->getCards()[1], &discard));
  EXPECT_FALSE(p1.validPlay(p1.getHand()->getCards()[2], &discard));
  EXPECT_TRUE(p1.validPlay(p1.getHand()->getCards()[3], &discard));

  p1.getHand()->insertCard(ek);
  EXPECT_FALSE(p1.validPlay(p1.getHand()->getCards()[4], &discard));
}

TEST(TestExplodingKittensPlayer, TestAnyValidPlays) {
  ExplodingKittensPlayer p1;

  Card* favor = new Card(Kind::favor);
  Card* attack = new Card(Kind::attack);

  CardPile* draw = new CardPile(100);
  CardPile* discard = new CardPile(100);

  EXPECT_FALSE(p1.anyValidPlays(discard));
  discard->insertCard(attack);
  p1.getHand()->insertCard(favor);
  EXPECT_TRUE(p1.anyValidPlays(discard));

  delete draw;
  draw = nullptr;
  delete discard;
  discard = nullptr;
}

TEST(TestExplodingKittensPlayer, TestPlayCard) {
  ExplodingKittensPlayer p1;

  CardPile* discard = new CardPile(100);
  EXPECT_THROW(p1.playCard(1, discard), illegal_player_operation);
  EXPECT_THROW(p1.playCard(-1, discard), illegal_player_operation);

  Card* attack = new Card(Kind::attack);
  p1.getHand()->insertCard(attack);
  EXPECT_EQ(0, discard->getSize());
  p1.playCard(0, discard);
  EXPECT_EQ(0, p1.getHandSize());
  EXPECT_EQ(1, discard->getSize());

  delete discard;
  discard = nullptr;
}
