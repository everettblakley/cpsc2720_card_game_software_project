/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CardGame.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>

/**
* Display text from a file, whether it be game rules or a user manual
* @param filePath The path to take to the file containing the text
*/
void CardGame::displayText(std::string filePath) {
  std::ifstream fin;
  fin.open(filePath);
  std::string textLine;

  if (fin) {
    std::cout << std::endl;
    while (!fin.eof()) {
    std::getline(fin, textLine, '\n');
    std::cout << textLine << std::endl;
    }
    fin.close();
  }
}

/**
* Starts a game from scratch
*/
void CardGame::newGame() {
  this->deal();
  this->playGame();
  char garbage;
  std:: cout << "Enter any character to go back to the Main Menu: ";
  std::cin >> garbage;
  std::cin.ignore(100, '\n');
  std::cout << std::endl;
  system("clear");
}
