/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CrazyEightsPlayer.h"

/**
* Destructor for a CrazyEightsPlayer
*/
CrazyEightsPlayer::~CrazyEightsPlayer() {
  delete hand;
  hand = nullptr;
}

/**
* Check to see if a certain Card is a valid play
* @param card The Card to be examined
* @param pile The CardPile to be compared to (top Card)
* @return True if the play is valid, false otherwise
*/
bool CrazyEightsPlayer::validPlay(const Card* card, CardPile* pile) {
  if (card->getSuit() == Suit::exploding_kitten) {
    return false;
  } else if (card->getRank() == 8) {
    return true;
  } else if (card->getSuit() == pile->top()->getSuit()
  || card->getRank() == pile->top()->getRank()) {
    return true;
  } else {
    return false;
  }
}

/**
* Draws a card from the top of a CardPile
* @param pile The CardPile to draw from
*/
void CrazyEightsPlayer::drawCard(CardPile* pile) {
  Player::drawCard(pile);
  if (!isComputer) {
    std::cout << "You picked up a ";
    std::cout << *(hand->getCards()[this->getHandSize()-1]) << std::endl;
  }
}
