/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "ExplodingKittens.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "Card.h"

/**
* Constructor for an Exploding Kittens game. Sets file paths and
* intializes players
*/
ExplodingKittens::ExplodingKittens() {
  players.push_back(new ExplodingKittensPlayer());
  players.push_back(new ExplodingKittensPlayer(true));
  drawPile = new CardPile(51);
  discardPile = new CardPile(51);
  saveFilePath = "docs/user/ExplodingKittensSave.txt";
  rulesFilePath = "docs/user/ExplodingKittensRules.txt";
}
/**
* Destructor for an Exploding Kitten
*/
ExplodingKittens::~ExplodingKittens() {
  delete drawPile;
  drawPile = nullptr;
  delete discardPile;
  discardPile = nullptr;
}

/**
* Displays the main menu for an Exploding Kittens game, allowing saving,
* loading, seeing the rules of the game and quitting
*/
void ExplodingKittens::mainMenu() {
  int choice;
  bool quit = false;
  do {
    std::cout << "========= EXPLODING KITTENS =========" << std::endl;
    std::cout << "============= MAIN MENU =============" << std::endl;
    std::cout <<  "Welcome to Exploding Kittens!" << std::endl;
    std::cout <<  "What would you like to do?" << std::endl;
    std::cout << "\t 1. New Game" << std::endl;
    std::cout << "\t 2. Load Game" << std::endl;
    std::cout << "\t 3. Help" << std::endl;
    std::cout << "\t 4. Quit" << std::endl;
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    std::cout << "====================================" << std::endl;
    switch (choice) {
      case 1: this->newGame(); break;
      case 2: this->loadGame(); break;
      case 3: this->displayText(rulesFilePath); break;
      case 4: quit = true; break;
      default: std::cout << "Invalid Option" << std::endl;
    }
    this->resetGame();
  } while (!quit);
  std::cout << "Thanks for playing Exploding Kittens! :)";
  std::cout << std::endl << std::endl;
}

void ExplodingKittens::playGame() {
  while (!gameOver) {
    this->playerTurn(players[turnCount % 2]);
    this->checkGameOver();
  }
}

/**
* Saves the game in its current state to the save file
*/
void ExplodingKittens::saveGame() {
  std::ofstream output("docs/user/ExplodingKittensSave.txt");
  for (int i = 0; i < players.size(); i++) {
    for (int j = 0; j < players[i]->getHandSize(); j++) {
      output << *(players[i]->getHand()->getCards()[j]) << "#";
    }
    output << "|";
  }
  for (int j = 0; j < drawPile->getSize(); j++) {
    output << *(drawPile->getCards()[j]) << "#";
  }
  output << "|";
  for (int k = 0; k < discardPile->getSize(); k++) {
    output << *(discardPile->getCards()[k]) << "#";
  }
  output << "|";
  output << turnCount;
  output.close();
  std::cout << std::endl << "Game saved successfully!" << std::endl
  << std::endl;
}

/**
* Loads a game state from a save file
*/
void ExplodingKittens::loadGame() {
  std::ifstream input("docs/user/ExplodingKittensSave.txt");
  if (input) {
    std::string data;
    int lineCount = 0;
    while (!input.eof()) {
      std::cout << "Readling line " << lineCount
                << " of save file" << std::endl;
      std::getline(input, data, '|');
      std::stringstream line(data);
      std::string inCard;
      while (std::getline(line, inCard, '#')) {
        if (lineCount < 4) {
          Kind kind;
          if (inCard == "Exploding Kitten") {
            kind = Kind::exploding_kitten;
          } else if (inCard == "Defuse") {
            kind = Kind::defuse;
          } else if (inCard == "Nope") {
            kind = Kind::nope;
          } else if (inCard == "Attack") {
            kind = Kind::attack;
          } else if (inCard == "Skip") {
            kind = Kind::skip;
          } else if (inCard == "Favor") {
            kind = Kind::favor;
          } else if (inCard == "Shuffle") {
            kind = Kind::shuffle;
          } else if (inCard == "See the Future") {
            kind = Kind::see_future;
          } else if (inCard == "Cat") {
            kind = Kind::cat;
          }
          Card* c = new Card(kind);
          switch (lineCount) {
            case 0: players[0]->getHand()->insertCard(c); break;
            case 1: players[1]->getHand()->insertCard(c); break;
            case 2: drawPile->insertCard(c); break;
            case 3: discardPile->insertCard(c); break;
          }
        } else {
          this->turnCount = std::stoi(inCard);
        }
      }
      lineCount++;
    }
    input.close();
    this->playGame();
  } else {
    std::cout << "Unable to read save file. Starting new game...";
    std::cout << std::endl << std::endl;
    this->newGame();
  }
}

/**
* Searches for an Exploding Kitten in a players hand, and prompts the user to
* defuse it if there is one found.
*/
void ExplodingKittens::checkGameOver() {
  for (int i = 0; i < players.size(); i++) {
    if (players[i]->getHand()->getCards()[players[i]->getHandSize()-1]
    ->getKind() == Kind::exploding_kitten) {
      std::cout << std::endl << "EXPLODING KITTEN DETECTED. DEFUSE IMMEDIATELY"
      << std::endl << std::endl;
      bool hasDefuse = false;
      unsigned int defuseIndex;
      for (int k = 0; k < players[i]->getHandSize(); k++) {
        if (players[i]->getHand()->getCards()[k]->getKind()
        == Kind::defuse) {
          hasDefuse = true;
          defuseIndex = k;
        }
      }
      if (hasDefuse) {
        this->kittenProcedure(players[i], defuseIndex);
      } else {
        std::cout << "NO DEFUSE. EXPLOSION IMMENENT. GOODBYE" << std::endl
        << std::endl;
        if (players[i]->isComputer) {
          std::cout << "CPU opponent Exploded! You WIN!";
        } else {
          std::cout << "You Exploded! CPU opponent WINS!";
        }
        std::cout << std::endl << std::endl;
        gameOver = true;
      }
    }
  }
}

/**
* Displays an in-game menu with a list of player actions
*/
void ExplodingKittens::menu() {
  char choice;
  bool quitTurn = false;
  bool turnEnded = false;
  bool truth = true;
  do {
    std::cout << std::endl << "Enter any character to continue: ";
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    system("clear");
    std::cout << "============= Your Turn ============";
    std::cout << std::endl;
    std::cout << "Your hand: ";
    for (int i = 0; i < players[0]->getHandSize(); i++) {
      std::cout << *(players[0]->getHand()->getCards()[i]) << ", ";
    }
    std::cout << std::endl;

    std::cout << std::endl << "What would you like to do?" << std::endl;
    std::cout << "\t C. Play a Card" << std::endl;
    std::cout << "\t P. Pass - draw a card and end your turn" << std::endl;
    std::cout << "\t S. Save Game" << std::endl;
    std::cout << "\t R. See Rules" << std::endl;
    std::cout << "\t Q. Quit Game - Return to Main Menu" << std::endl;
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    std::cout << "====================================" << std::endl;
    switch (choice) {
      case 'C':
      case 'c': this->playing(&turnEnded); if (turnEnded == true) {
        std::cout << "Here" << std::endl;
       quitTurn = true;
      } break;
      case 'P':
      case 'p': this->playing(&truth); quitTurn = true; break;
      case 'S':
      case 's': this->saveGame(); break;
      case 'R':
      case 'r': this->displayText(rulesFilePath); break;
      case 'Q':
      case 'q': quitTurn = true; gameOver = true; break;
      default: std::cout << "Invalid Option" << std::endl << std::endl;
    }
  } while (!quitTurn);
}

/**
* Deals cards to each of the players, then gives them each defuse cards and
* places the exploding_kitten randomly within the drawPile
*/
void ExplodingKittens::deal() {
  // Load 5 Nopes
  for (int i = 0; i < 5; i++) {
    Card* nope = new Card(Kind::nope);
    drawPile->insertCard(nope);
  }
  // Load 4 Attacks
  for (int j = 0; j < 4; j++) {
    Card* attack = new Card(Kind::attack);
    drawPile->insertCard(attack);
  }
  // Load 4 Skips
  for (int k = 0; k < 4; k++) {
    Card* skip = new Card(Kind::skip);
    drawPile->insertCard(skip);
  }
  // Load 4 Favors
  for (int m = 0; m < 4; m++) {
    Card* favor = new Card(Kind::favor);
    drawPile->insertCard(favor);
  }
  // Load 4 Shuffles
  for (int n = 0; n < 4; n++) {
    Card* shuffle = new Card(Kind::shuffle);
    drawPile->insertCard(shuffle);
  }
  // Load 5 See the Futures
  for (int x = 0; x < 5; x++) {
    Card* sf = new Card(Kind::see_future);
    drawPile->insertCard(sf);
  }
  // Load 20 Cats
  for (int y = 0; y < 20; y++) {
    Card* cat = new Card(Kind::cat);
    drawPile->insertCard(cat);
  }
  drawPile->shuffle();
  for (int i = 0; i < players.size(); i++) {
    for (int j  = 0; j < 7; j++) {
      players[i]->drawCard(drawPile);
    }
    Card* defuse = new Card(Kind::defuse);
    players[i]->getHand()->insertCard(defuse);
  }
  std::cout << "Everyone was dealt a Defuse." << std::endl;
  Card* ek = new Card(Kind::exploding_kitten);
  drawPile->insertCard(ek);
  drawPile->shuffle();
}

/**
* Reset all the values of the game back to the original state, used if the
* user decides to quit at any point
*/
void ExplodingKittens::resetGame() {
  std::cout << "Cleaning up the game... and the players who exploded..."
  << std::endl;
  CardPile garbage(100);
  for (int i = 0; i < players.size(); i++) {
    while (players[i]->getHandSize() > 0) {
      garbage.insertCard(players[i]->getHand()->pop());
    }
  }
  while (drawPile->getSize() > 0) {
    garbage.insertCard(drawPile->pop());
  }
  while (discardPile->getSize() > 0) {
    garbage.insertCard(discardPile->pop());
  }
  gameOver = false;
  turnCount = 0;
  std::cout << "Clean!" << std::endl;
}

/**
* The player turn rotation for a player. Plays the computer turn for them
* @param player The player whos turn it is
*/
void ExplodingKittens::playerTurn(ExplodingKittensPlayer* player) {
  this->checkGameOver();
  if (!gameOver) {
    if (player->isComputer) {
      std::cout << std::endl << "---------- Computer's Turn ---------";
      std::cout << std::endl;
      if (player->anyValidPlays(discardPile)) {
        int i = 0;
        while (!(player->validPlay(player->getHand()->getCards()[i],
          discardPile)) && player->getHand()->getCards()[i]->getKind()
          == Kind::nope) {
          i++;
        }
        player->playCard(i, discardPile);
        std::cout << "\t Computer played a ";
        std::cout << *(discardPile->top());
        std::cout << std::endl;
        if (!(this->nope(players[0]))) {
          this->performCardAction(players[1], players[0], discardPile->top());
        } else {
          std::cout << "You Nope'd them hard" << std::endl;
        }
      } else {
        std::cout << "Computer has no valid plays" << std::endl;
        std::cout << "\t Picking up a card" << std::endl;
        player->drawCard(drawPile);
        std::cout << "\t They draw a card" << std::endl;
        if (player->getHand()->getCards()[player->getHandSize()-1]->getKind()
          == Kind::exploding_kitten) {
            std::cout << "Computer picked up an Exploding Kitten!" << std::endl;
          }
      }
      std::cout << "\t Comptuter has ended their turn." << std::endl;
      std::cout << "------------------------------------" << std::endl;
    } else {
      std::cout << "Your Turn!" << std::endl;
      if (!(player->anyValidPlays(discardPile))) {
        std::cout << "You do now have any valid cards to play!" << std::endl;
        char c;
        std::cout << std::endl
        << "Enter any character to draw and end your turn: ";
        std::cin >> c;
        std::cin.ignore(100, '\n');
        player->drawCard(drawPile);
      } else {
        this->menu();
      }
    }
    turnCount++;
  }
}

/**
* The action function for a human player, allows them to draw or play a card
*/
void ExplodingKittens::playing(bool* pass) {
  if (!*(pass)) {
    if (!(players[0]->anyValidPlays(discardPile))) {
      std::cout << "You do now have any valid cards to play!" << std::endl;
      char c;
      std::cout << std::endl
      << "Enter any character to draw and end your turn: ";
      std::cin >> c;
      std::cin.ignore(100, '\n');
      players[0]->drawCard(drawPile);
    } else {
      players[0]->playCard(this->promptForPlay(discardPile), discardPile);
      *(pass) = this->performCardAction(players[0], players[1],
         discardPile->top());
    }
  } else {
    std::cout << "You passed. Drawing a Card" << std::endl;
    players[0]->drawCard(drawPile);
  }
}

/**
* The procedure for when a Kitten is picked up and a defuse is avaliable
* @param player The player who will defuse the kitten and relocate the
* Exploding Kitten.
* @param dIndex The index of the defuse to be removed, used to save time
*/
void ExplodingKittens::kittenProcedure(ExplodingKittensPlayer* player,
  int dIndex) {
  discardPile->insertCard(player->getHand()->removeCard(dIndex));
  Card* ek = player->getHand()->removeCard(player->getHandSize() - 1);
  if (player->isComputer) {
    std::cout << "\t Computer defused a Kitten. Reinserting..."
    << std::endl << std::endl;
    drawPile->insertCard(ek, 0);
  } else {
    int placement;
    std::cout << "\t You had a defuse!" << std::endl << std::endl
    << "You defused the kitten. Where would you like to place it?"
    << std::endl;
    do {
      std::cout << "(Between 0 and " << drawPile->getSize()
      << ", where " << drawPile->getSize() << " is the top): ";
      std::cin >> placement;
    } while (placement < 0 || placement > drawPile->getSize());
    drawPile->insertCard(ek, placement);
    std::cout << "Exploding Kitten armed and reinserted..." << std::endl;
  }
}

/**
* Function for when a player is able to "nope" another player's play
* @param p The player who might "nope" a play
*/
bool ExplodingKittens::nope(ExplodingKittensPlayer* p) {
  if (!p->isComputer) {
    for (int i = 0; i < p->getHandSize(); i++) {
      if (p->getHand()->getCards()[i]->getKind() == Kind::nope &&
      p->validPlay(p->getHand()->getCards()[i], discardPile)) {
        char nope;
        do {
          std::cout << "\t Would you like to Nope their play? (y/n): ";
          std::cin >> nope;
        } while (nope != 'y' && nope != 'n');
        if (nope == 'y') {
          p->playCard(i, discardPile);
          return true;
        } else {
          return false;
        }
      }
    }
    return false;
  } else {
    return false;
  }
}

/**
* Perform an action form one of various action cards
* @param p The Player performing the action
* @param c The Card to be played
*/
bool ExplodingKittens::performCardAction(ExplodingKittensPlayer* p,
  ExplodingKittensPlayer* opp, Card* c) {
  Kind k = c->getKind();
  switch (k) {
    case Kind::attack: {
      std::cout << "Attack played! Making the opponent pick up your card!"
      << std::endl;
      opp->drawCard(drawPile);
      return true;
    }
    case Kind::skip: {
      std::cout << "Skip played! Ending the players turn!" << std::endl;
      return true;
    }
    case Kind::favor: {
      if (p->isComputer) {
        std::cout << "Computer played a Favor!" << std::endl;
        std::cout << "What card would you like to give them?" << std::endl;
        for (int i = 0; i < players[0]->getHandSize(); i++) {
          std::cout << "\t " << i << ". ";
          std::cout << *(players[0]->getHand()->getCards()[i]) << std::endl;
        }
        int choice;
        do {
          std::cout << "Index: ";
          std::cin >> choice;
        } while (choice < 0 || choice >= players[0]->getHandSize());
        Card* removedCard = players[0]->getHand()->removeCard(choice);
        std::cout << "\t How gracious of you. You gave them a ";
        std::cout << *(removedCard) << std::endl;
        discardPile->insertCard(removedCard);
        p->drawCard(discardPile);

      } else {
        std::cout << "You played a Favor!" << std::endl;
        std::cout << "Computer is picking a card, dilligently" << std::endl;
        Card* givenCard = players[1]->getHand()->removeCard(0);
        p->getHand()->insertCard(givenCard);
        std::cout << "\t They have gifted you a " << *givenCard << std::endl;
      }
      return false;
    }
    case Kind::shuffle: {
      drawPile->shuffle();
      std::cout << "Drawpile has been shuffled!" << std::endl;
      return false;
    }
    case Kind::see_future: {
      if (!p->isComputer) {
        std::cout << "See The Future!" << std::endl;
        std::cout << "The next cards are: " << std::endl;
        int size = drawPile->getSize();
        int cardsDisplayed = 0;
        while (size > 0 && cardsDisplayed < 3) {
          std::cout << "\t " << cardsDisplayed + 1<< ". ";
          std::cout << *(drawPile->getCards()[size - 1]) << std::endl;
          size--;
          cardsDisplayed++;
        }
      }
      return false;
    }
    default: return false;
  }
}

/**
* Prompts the player for a card to play (when they want to play a card)
* @param cp The CardPile to check the validity of a play
*/
int ExplodingKittens::promptForPlay(CardPile* cp) {
  int index;
  do {
    std::cout << "Please enter the index of the card you want to play";
    std::cout << std::endl;
    for (int i = 0; i < players[0]->getHandSize(); i++) {
      if (players[0]->validPlay(players[0]->getHand()->getCards()[i],
    discardPile)) {
      std::cout << "\t " << i << ". ";
      std::cout << *(players[0]->getHand()->getCards()[i]);
      std::cout << std::endl;
      }
    }
    std::cout << "Index: ";
    std::cin >> index;
  } while (index < 0 || index >= players[0]->getHandSize());
  return index;
}
