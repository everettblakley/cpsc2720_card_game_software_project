/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "GoFish.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "Exceptions.h"
#include "CardPile.h"

/**
* Constructor for a GoFish game
*/
GoFish::GoFish() {
  GoFishPlayer* p1 = new GoFishPlayer();
  players.push_back(p1);
  GoFishPlayer* p2 = new GoFishPlayer(true);
  players.push_back(p2);

  drawPile = new CardPile(52);
  rulesFilePath = "docs/user/GoFishRules.txt";
  saveFilePath = "docs/user/GoFishSave.txt";
}

/**
* Destructor for a GoFish game
*/
GoFish::~GoFish() {
  delete drawPile;
  drawPile = nullptr;
  for (int i = 0; i < players.size(); i++) {
    delete players[i];
    players[i] = nullptr;
  }
}

/**
* Displays the main menu for a GoFish game
*/
void GoFish::mainMenu() {
  int choice;
  bool quit = false;
  do {
    std::cout << "============= GO FISH ==============" << std::endl;
    std::cout << "============ MAIN MENU =============" << std::endl;
    std::cout << "Welcome to Go Fish!" << std::endl;
    std::cout << "What would you like to do?" << std::endl;
    std::cout << "\t 1. New Game" << std::endl;
    std::cout << "\t 2. Load Game" << std::endl;
    std::cout << "\t 3. Help" << std::endl;
    std::cout << "\t 4. Quit" << std::endl;
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    std::cout << "====================================" << std::endl;
    switch (choice) {
      case 1: this->newGame(); break;
      case 2: this->loadGame(); break;
      case 3: this->displayText(rulesFilePath); break;
      case 4: quit = true; break;
      default: std::cout << "Invalid Option" << std::endl << std::endl;
    }
    if (gameOver) {
      this->scoreboard();
    }
    this->resetGame();
  } while (!quit);
  std::cout << "Thanks for playing Go Fish! :)" << std::endl;
}

/**
* Begins the game process after creating or loading a game
*/
void GoFish::playGame() {
  while (!gameOver) {
    this->playerTurn(players[turnCount % 2], players[(turnCount + 1) % 2]);
    this->checkGameOver();
  }
}

/**
* Saves a game in it's current state, storing players' hands, books
* and the deck of cards
*/
void GoFish::saveGame() {
  /*
  * Output format:
  * Player 1's hand
  * Player 1's books
  * Player 2's hand
  * Player 2's books
  * DrawPile contents
  * Turn Count
  */
  std::ofstream output(saveFilePath);
  for (int i = 0; i < players.size(); i++) {
    output << *(players[i]->getHand()) << "|";
    std::vector<Card*> book = *(players[i]->getBooks());
    for (int j = 0; j < book.size(); j++) {
      output << *(book[j]) << " ";
    }
    output << "|";
  }
  for (int k = 0; k < drawPile->getSize(); k++) {
    output << *(drawPile->getCards()[k]) << " ";
  }
  output << "|";
  output << turnCount;
  output.close();
  std::cout << std::endl << "Game saved successfully!" << std::endl
  << std::endl;
}

/**
* Loads a game state from a file and begins turn rotation
*/
void GoFish::loadGame() {
  std::ifstream input(saveFilePath);
  if (input) {
    std::string data;
    int lineCount = 0;
    while (!input.eof()) {
      std::cout << "Readling line " << lineCount
                << " of save file" << std::endl;
      std::getline(input, data, '|');
      std::stringstream line(data);
      std::string inCard;
      while (std::getline(line, inCard, ' ')) {
        if (lineCount < 5) {
          char inSuit = inCard.back();
          inCard.pop_back();
          Suit suit;
          switch (inSuit) {
            case 'C': suit = Suit::clubs; break;
            case 'D': suit = Suit::diamonds; break;
            case 'H': suit = Suit::hearts; break;
            case 'S': suit = Suit::spades; break;
          }
          int rank;
          if (inCard == "K") {
            rank = 13;
          } else if (inCard == "Q") {
            rank = 12;
          } else if (inCard == "J") {
            rank = 11;
          } else if (inCard == "A") {
            rank = 1;
          } else {
            rank = std::stoi(inCard);
          }
          Card* c = new Card(suit, rank);
          switch (lineCount) {
            case 0: players[0]->getHand()->insertCard(c); break;
            case 1: players[0]->getBooks()->push_back(c); break;
            case 2: players[1]->getHand()->insertCard(c); break;
            case 3: players[1]->getBooks()->push_back(c); break;
            case 4: drawPile->insertCard(c); break;
          }
        } else {
          this->turnCount = std::stoi(inCard);
        }
      }
      lineCount++;
    }
    input.close();
    this->playGame();
  } else {
    std::cout << "Unable to read save file. Starting new game...";
    std::cout << std::endl << std::endl;
    this->newGame();
  }
}

/**
* Checks to see if the game has been completed
*/
void GoFish::checkGameOver() {
  if (players[0]->getHandSize() == 0 && players[1]->getHandSize() == 0) {
    gameOver = true;
  }
}

/**
* Displays a menu for a GoFish player, allowing them to perform
* various actions
*/
void GoFish::menu() {
  char choice;
  bool quitTurn = false;
  do {
    std::cout << std::endl << "Enter any character to continue: ";
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    system("clear");
    std::cout << "============= Your Turn ============" << std::endl;
    std::cout << "Your hand: " << *(players[0]->getHand()) << std::endl
    << std::endl;

    std::cout << "What would you like to do?" << std::endl;
    std::cout << "\t F. Fish for a card" << std::endl;
    std::cout << "\t V. View Scores" << std::endl;
    std::cout << "\t S. Save Game" << std::endl;
    std::cout << "\t R. See Rules" << std::endl;
    std::cout << "\t Q. Quit Game - Return to Main Menu" << std::endl;
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    std::cout << "====================================" << std::endl;
    switch (choice) {
      case 'F':
      case 'f': this->fishing(&quitTurn); break;
      case 'V':
      case 'v': this->scoreboard(); break;
      case 'S':
      case 's': this->saveGame(); break;
      case 'R':
      case 'r': this->displayText(rulesFilePath); break;
      case 'Q':
      case 'q': quitTurn = true; gameOver = true; break;
      default: std::cout << "Invalid Option" << std::endl << std::endl;
    }
  } while (!quitTurn);
}

/**
* Deals card to each player after initializing the draw pile
* Checks for immediate books that may have formed
*/
void GoFish::deal() {
  std::cout << "Dealing the cards..." << std::endl << std::endl;
  for (int i = 0; i < drawPile->getMaxSize()/4; i++) {
    Card* club = new Card(Suit::clubs, i+1);
    drawPile->insertCard(club);
    Card* diamond = new Card(Suit::diamonds, i+1);
    drawPile->insertCard(diamond);
    Card* heart = new Card(Suit::hearts, i+1);
    drawPile->insertCard(heart);
    Card* spade = new Card(Suit::spades, i+1);
    drawPile->insertCard(spade);
  }
  drawPile->shuffle();

  for (int k = 0; k < players.size(); k++) {
    for (int l = 0; l < 7; l++) {
      players[k]->drawCard(drawPile);
    }
  }
}

/**
* Reset the parameters of the CardGame
*/
void GoFish::resetGame() {
  std::cout << "Cleaning up the game..." << std::endl;

  CardPile garbage(52);
  for (int i = 0; i < players.size(); i++) {
    while (players[i]->getHandSize() > 0) {
      garbage.insertCard(players[i]->getHand()->pop());
    }
    players[i]->clearBooks();
  }

  while (drawPile->getSize() > 0) {
    garbage.insertCard(drawPile->pop());
  }

  gameOver = false;
  turnCount = 0;

  std::cout << "Clean!" << std::endl;
}

/**
* Begins a turn sequence for an individual player, or a pre-made turn
* sequence for a computer opponent
* @param player The current player
* @param opp That player's opponent
*/
void GoFish::playerTurn(GoFishPlayer* player, GoFishPlayer* opp) {
  if (player->isComputer) {
    std::cout << std::endl
    << "---------- Computer's Turn ---------" << std::endl;
    bool fishing = true;
    while (fishing && !gameOver) {
      std::cout << "Computer is fishing for ";
      std::cout << *(player->getHand()->getCards()[0]) << std::endl;
      fishing = player->fish(opp, player->getHand()->getCards()[0]);
      this->checkHand(player);
    }
    if (!gameOver) {
      std::cout << "\t Computer went fishing. Drawing a card..." << std::endl;
      player->drawCard(drawPile);
      this->checkHand(player);

    } else {
      std::cout << "Game is over!" << std::endl;
    }
    std::cout << "------------------------------------" << std::endl
    << std::endl;
  } else {
    this->menu();
  }
  turnCount++;
}

/**
* Displays a scoreboard showing number of books for each player
*/
void GoFish::scoreboard() {
  std::cout << std::endl;
  std::cout << "********* SCOREBOARD *********" << std::endl << std::endl;
  std::cout << "* You: " << players[0]->getScore() << " book(s)."
            << std::endl << std::endl;
  std::cout << "* CPU: " << players[1]->getScore() << " book(s)."
            << std::endl << std::endl;
  std::cout << "******************************" << std::endl << std::endl;

  if (gameOver) {
    std::cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+" << std::endl << std::endl;
    if (players[0]->getScore() > players[1]->getScore()) {
      std::cout << "\t  You win!" << std::endl << std::endl;
    } else if (players[1]->getScore() > players[0]->getScore()) {
      std::cout << "\t CPU wins!" << std::endl << std::endl;
    } else {
      std::cout << "\t  Tied!" << std::endl << std::endl;
    }
    std::cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+" << std::endl << std::endl
              << std::endl;
  }
}

/**
* To go fishing for a card within the confines of a GoFish game
* @param wentFishing True if the player
*/
void GoFish::fishing(bool* wentFishing) {
  this->checkGameOver();
  if (!gameOver) {
    int index;
    do {
      std::cout << "Please enter the index to fish for: " << std::endl;
      for (int i = 0; i < players[0]->getHandSize(); i++) {
        std::cout << "\t" << i << ". ";
        std::cout << *(players[0]->getHand()->getCards()[i]);
        std::cout << std::endl;
      }
      std::cout << "Index: ";
      std::cin >> index;
    } while (index < 0 || index >= players[0]->getHandSize());

    Card* fishCard = players[0]->getHand()->getCards()[index];
    std::cout << "\t You're fishing for a " << *fishCard << std::endl;
    if (players[0]->fish(players[1], fishCard)) {
      std::cout << "\t Fish successful! Go again! " << std::endl;
      this->checkGameOver();
      if (gameOver) {
        *wentFishing = true;
      }
    } else {
      std::cout << "Go Fish! Enter any character to draw a card: ";
      char garbage;
      std::cin >> garbage;
      std::cin.ignore(100, '\n');
      players[0]->drawCard(this->drawPile);
      *(wentFishing) = true;
    }
    this->checkHand(players[0]);
  } else {
    *wentFishing = true;
  }
}

/**
* Checks a players hand for certain conditions
*/
void GoFish::checkHand(GoFishPlayer* player) {
  this->checkGameOver();
  if (!gameOver && player->getHandSize() == 0) {
    std::cout << "A Hand is empty! Refiling it..." << std::endl;
    for (int i = 0; i < 7; i++) {
      if (!(drawPile->getSize() == 0)) {
        player->drawCard(drawPile);
      } else {
        break;
      }
    }
    std::cout << "Filled Hand." << std::endl;
  }
}
