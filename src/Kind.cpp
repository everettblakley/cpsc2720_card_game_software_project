/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "Kind.h"
#include "Exceptions.h"
#include <iostream>

std::ostream& operator<< (std::ostream& os, const Kind& kind) {
  switch (kind) {
    case Kind::exploding_kitten: os << "Exploding Kitten"; break;
    case Kind::defuse: os << "Defuse"; break;
    case Kind::nope: os << "Nope"; break;
    case Kind::attack: os << "Attack"; break;
    case Kind::skip: os << "Skip"; break;
    case Kind::favor: os << "Favor"; break;
    case Kind::shuffle: os << "Shuffle"; break;
    case Kind::see_future: os << "See the Future"; break;
    case Kind::cat: os << "Cat"; break;
  }
  return os;
}
