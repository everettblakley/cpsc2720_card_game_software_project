/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include <stdio.h>
#include <iostream>
#include "GoFish.h"
#include "CrazyEights.h"
#include "ExplodingKittens.h"

int main() {
  GoFish goFish;
  CrazyEights crazy8;
  ExplodingKittens ekg;
  int selection;
  bool quit = false;
  system("clear");
  do {
    std::cout    << "--------------------------------------------------"
    << std::endl << "|      TEXT BASED CARD GAMES - TEAM GANDALF      |"
    << std::endl << "|      ~ Everett Blakley and Logan Wilkie ~      |"
    << std::endl << "--------------------------------------------------"
    << std::endl;
    std::cout << "Which game would you like to play?" << std::endl;
    std::cout << "\t 1. Go Fish" << std::endl;
    std::cout << "\t 2. Crazy Eights" << std::endl;
    std::cout << "\t 3. Exploding Kittens (Prototype)" << std::endl;
    std::cout << "\t 4. Quit" << std::endl;
    std::cout << "\t 5. User Manual" << std::endl;
    std::cin >> selection;
    std::cin.ignore(100, '\n');

    switch (selection) {
      case 1: goFish.mainMenu(); system("clear"); break;
      case 2: crazy8.mainMenu(); system("clear"); break;
      case 3: ekg.mainMenu(); system("clear"); break;
      case 4: quit = true; break;
      case 5: goFish.displayText("docs/user/userManual.txt"); break;
      default: std::cout << "Invalid Option" << std::endl;
    }
  } while (!quit);
  std::cout << "Thanks for playing! :)" << std::endl << std::endl;
  return 0;
}
