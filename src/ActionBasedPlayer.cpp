/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "ActionBasedPlayer.h"
#include <iostream>

/**
* Plays a Card to the CardPile
* @param card The Card to be played, if valid
* @param pile The CardPile the Card is played to
*/
void ActionBasedPlayer::playCard(unsigned int index, CardPile* pile) {
  if (index < 0 || index > this->getHandSize()) {
    throw illegal_player_operation("Not a valid index to play a card");
  } else {
    pile->insertCard(hand->removeCard(index));
  }
}

/**
* Check a Player objects Hand to see if they are able to make any plays
* @param cp The CardPile that the Card is being tested validity against
* @return True if there is at least 1 valid play, false otherwise
*/
bool ActionBasedPlayer::anyValidPlays(CardPile* cp) {
  for (unsigned int i = 0; i < hand->getSize(); i++) {
    if (this->validPlay(hand->getCards()[i], cp)) {
      return true;
    }
  }
  return false;
}
