/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CrazyEights.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "Exceptions.h"
#include "Card.h"

/**
* Constructor for a CrazyEights game. Initializes players and file paths
*/
CrazyEights::CrazyEights() {
  CrazyEightsPlayer* p1 = new CrazyEightsPlayer();
  players.push_back(p1);
  CrazyEightsPlayer* p2 = new CrazyEightsPlayer(true);
  players.push_back(p2);

  drawPile = new CardPile(52);
  discardPile = new CardPile(52);
  rulesFilePath = "docs/user/CrazyEightsRules.txt";
  saveFilePath = "docs/user/CrazyEightsSave.txt";
}

/**
* Destructor for a CrazyEights game
*/
CrazyEights::~CrazyEights() {
  delete drawPile;
  drawPile = nullptr;
  delete discardPile;
  discardPile = nullptr;

  for (int i = 0; i < players.size(); i++) {
    delete players[i];
    players[i] = nullptr;
  }
}

/**
* Displays a mainMenu for the game, allowing players to create, load,
* get help and quit a game.
*/
void CrazyEights::mainMenu() {
  int choice;
  bool quit = false;
  do {
    std::cout << "=========== CRAZY EIGHTS ============" << std::endl;
    std::cout << "============= MAIN MENU =============" << std::endl;
    std::cout <<  "Welcome to Crazy Eights!" << std::endl;
    std::cout <<  "What would you like to do?" << std::endl;
    std::cout << "\t 1. New Game" << std::endl;
    std::cout << "\t 2. Load Game" << std::endl;
    std::cout << "\t 3. Help" << std::endl;
    std::cout << "\t 4. Quit" << std::endl;
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    std::cout << "====================================" << std::endl;
    switch (choice) {
      case 1: this->newGame(); break;
      case 2: this->loadGame(); break;
      case 3: this->displayText(rulesFilePath); break;
      case 4: quit = true; break;
      default: std::cout << "Invalid Option" << std::endl;
    }
    if (gameOver) {
      this->scoreboard();
    }
    this->resetGame();
  } while (!quit);
  std::cout << "Thanks for playing Crazy Eights! :)" << std::endl << std::endl;
}

/**
* Begins the turn roation sequence, and checks for the end of each game
*/
void CrazyEights::playGame() {
  while (!gameOver) {
    this->playerTurn(players[turnCount % 2]);
    this->checkGameOver();
  }
}

/**
* Saves a game in its current state to a local text file
*/
void CrazyEights::saveGame() {
  /*
  * Output format:
  * Player 1's hand
  * Player 2's hand
  * DrawPile contents
  * DiscardPile contents
  * Turn Count
  */
  std::ofstream output(saveFilePath);
  for (int i = 0; i < players.size(); i++) {
    output << *(players[i]->getHand()) << "|";
  }
  for (int j = 0; j < drawPile->getSize(); j++) {
    output << *(drawPile->getCards()[j]) << " ";
  }
  output << "|";
  for (int k = 0; k < discardPile->getSize(); k++) {
    output << *(discardPile->getCards()[k]) << " ";
  }
  output << "|";
  output << turnCount;
  output.close();
  std::cout << std::endl << "Game saved successfully!" << std::endl
  << std::endl;
}

/**
* Loads a game state by coverting a text file to a game. Begins the game
*/
void CrazyEights::loadGame() {
  std::ifstream input(saveFilePath);
  if (input) {
    std::string data;
    int lineCount = 0;
    while (!input.eof()) {
      std::cout << "Readling line " << lineCount
                << " of save file" << std::endl;
      std::getline(input, data, '|');
      std::stringstream line(data);
      std::string inCard;
      while (std::getline(line, inCard, ' ')) {
        if (lineCount < 4) {
          char inSuit = inCard.back();
          inCard.pop_back();
          Suit suit;
          switch (inSuit) {
            case 'C': suit = Suit::clubs; break;
            case 'D': suit = Suit::diamonds; break;
            case 'H': suit = Suit::hearts; break;
            case 'S': suit = Suit::spades; break;
          }
          int rank;
          if (inCard == "K") {
            rank = 13;
          } else if (inCard == "Q") {
            rank = 12;
          } else if (inCard == "J") {
            rank = 11;
          } else if (inCard == "A") {
            rank = 1;
          } else {
            rank = std::stoi(inCard);
          }
          Card* c = new Card(suit, rank);
          switch (lineCount) {
            case 0: players[0]->getHand()->insertCard(c); break;
            case 1: players[1]->getHand()->insertCard(c); break;
            case 2: drawPile->insertCard(c); break;
            case 3: discardPile->insertCard(c); break;
          }
        } else {
          this->turnCount = std::stoi(inCard);
        }
      }
      lineCount++;
    }
    input.close();
    if (discardPile->top()->getRank() == 8) {
      currentSuit = discardPile->top()->getSuit();
    }
    this->playGame();
  } else {
    std::cout << "Unable to read save file. Starting new game...";
    std::cout << std::endl << std::endl;
    this->newGame();
  }
}

/**
* Checks the game for a game over condition, and changes the boolean
* variable accoridingly
*/
void CrazyEights::checkGameOver() {
  for (unsigned int i = 0; i < players.size(); i++) {
    if (players[i]->getHandSize() == 0) {
      gameOver = true;
      break;
    }
  }
}

/**
* Displays a menu in game, allowing players to play cards, save, view
* scores, see the rules and quit.
*/
void CrazyEights::menu() {
  char choice;
  bool quitTurn = false;
  do {
    std::cout << std::endl << "Enter any character to continue: ";
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    system("clear");
    std::cout << "============= Your Turn ============" << std::endl;
    std::cout << "Your hand: " << *(players[0]->getHand()) << std::endl
    << std::endl << "\t Current Card: " << *(discardPile->top()) << std::endl;
    if (discardPile->top()->getRank() == 8) {
      std::cout << "\t Suit after a Crazy Eight was played: " << currentSuit
      << std::endl;
    }

    std::cout << std::endl << "What would you like to do?" << std::endl;
    std::cout << "\t P. Play a Card" << std::endl;
    std::cout << "\t V. View # of Cards" << std::endl;
    std::cout << "\t S. Save Game" << std::endl;
    std::cout << "\t R. See Rules" << std::endl;
    std::cout << "\t Q. Quit Game - Return to Main Menu" << std::endl;
    std::cin >> choice;
    std::cin.ignore(100, '\n');
    std::cout << "====================================" << std::endl;
    switch (choice) {
      case 'P':
      case 'p': this->playing(); quitTurn = true; break;
      case 'V':
      case 'v': this->scoreboard(); break;
      case 'S':
      case 's': this->saveGame(); break;
      case 'R':
      case 'r': this->displayText(rulesFilePath); break;
      case 'Q':
      case 'q': quitTurn = true; gameOver = true; break;
      default: std::cout << "Invalid Option" << std::endl << std::endl;
    }
  } while (!quitTurn);
}

/**
* Intializes the deck, and deals cards to each player
*/
void CrazyEights::deal() {
  std::cout << "Dealing the cards..." << std::endl << std::endl;
  for (int i = 0; i < drawPile->getMaxSize()/4; i++) {
    Card* club = new Card(Suit::clubs, i+1);
    drawPile->insertCard(club);
    Card* diamond = new Card(Suit::diamonds, i+1);
    drawPile->insertCard(diamond);
    Card* heart = new Card(Suit::hearts, i+1);
    drawPile->insertCard(heart);
    Card* spade = new Card(Suit::spades, i+1);
    drawPile->insertCard(spade);
  }
  drawPile->shuffle();

  for (int k = 0; k < players.size(); k++) {
    for (int l = 0; l < 7; l++) {
      players[k]->drawCard(drawPile);
    }
  }
  discardPile->insertCard(drawPile->pop());

  if (discardPile->top()->getRank() == 8) {
    currentSuit = discardPile->top()->getSuit();
  }
}

/**
* Resets the attributes of the game after quitting or beginning a new game.
*/
void CrazyEights::resetGame() {
  std::cout << "Cleaning up the game..." << std::endl;
  CardPile garbage(52);
  for (int i = 0; i < players.size(); i++) {
    while (players[i]->getHandSize() > 0) {
      garbage.insertCard(players[i]->getHand()->pop());
    }
  }

  while (drawPile->getSize() > 0) {
    garbage.insertCard(drawPile->pop());
  }

  while (discardPile->getSize() > 0) {
    garbage.insertCard(discardPile->pop());
  }

  gameOver = false;
  turnCount = 0;
}

/**
* Begins a turn sequence for an individual player, or a pre-made turn
* sequence for a computer opponent
* @param p The player who is taking their turn
*/
void CrazyEights::playerTurn(CrazyEightsPlayer* p) {
  this->checkGameOver();
  if (!gameOver) {
    if (p->isComputer) {
      std::cout << std::endl << "---------- Computer's Turn ---------"
      << std::endl;
      CardPile* correctPile = discardPile;
      if (discardPile->top()->getRank() == 8) {
        CardPile* eightPile = new CardPile(1);
        Card eightCard(currentSuit, 8);
        eightPile->insertCard(&eightCard);
        correctPile = eightPile;
      }

      if (p->anyValidPlays(correctPile)) {
        for (unsigned int i = 0; i < p->getHandSize(); i++) {
          if (p->validPlay(p->getHand()->getCards()[i], correctPile)) {
            p->playCard(i, discardPile);
            std::cout << "Computer played a: " << *(discardPile->top())
            << std::endl;
            this->playedEight(p);
            break;
          }
        }

      } else {
        std::cout << "\t Computer has no valid plays. Drawing a card..."
        << std::endl;
        p->drawCard(drawPile);
        if (p->anyValidPlays(correctPile)
        && p->validPlay(p->getHand()->getCards()[p->getHandSize()-1],
        correctPile)) {
          std::cout << "\t Computer picked up a playable card!" << std::endl;
          p->playCard(p->getHandSize()-1, discardPile);
          std::cout << "Computer played a: " << *(discardPile->top())
          << std::endl;
          this->playedEight(p);
        }
      }
      std::cout << "------------------------------------" << std::endl;
    } else {
    this->menu();
    }
    turnCount++;
  }
}

/**
* View the scoreboard of the game, in this case the number of cards left
* by each player
*/
void CrazyEights::scoreboard() {
  std::cout << std::endl;
  std::cout << "********* SCOREBOARD *********" << std::endl << std::endl;
  std::cout << "* You: " << players[0]->getHandSize() << " card(s) left."
  << std::endl << std::endl;
  std::cout << "* CPU: " << players[1]->getHandSize() << " card(s) left."
  << std::endl << std::endl;
  std::cout << "******************************" << std::endl << std::endl;

  if (gameOver) {
    std::cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+" << std::endl << std::endl;
    if (players[0]->getHandSize() == 0) {
      std::cout << "\t  You win!" << std::endl << std::endl;
    } else if (players[1]->getHandSize() == 0) {
      std::cout << "\t CPU wins!" << std::endl << std::endl;
    }
    std::cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+" << std::endl << std::endl
              << std::endl;
  }
}

/**
* Functions used for when a human player is making their turn and decides to
* play a card
*/
void CrazyEights::playing() {
  this->checkGameOver();
  if (!gameOver) {
    CardPile* correctPile = discardPile;
    if (discardPile->top()->getRank() == 8) {
      CardPile* eightPile = new CardPile(1);
      Card eightCard(currentSuit, 8);
      eightPile->insertCard(&eightCard);
      correctPile = eightPile;
    }

    if (players[0]->anyValidPlays(correctPile)) {
      int index = promptForPlay(correctPile);
      players[0]->playCard(index, discardPile);
      this->playedEight(players[0]);
    } else {
      char garbage;
      std::cout << "No Valid Plays. Enter any character to draw a card: ";
      std::cin >> garbage;
      std::cin.ignore(100, '\n');
      players[0]->drawCard(drawPile);
      if (players[0]->anyValidPlays(correctPile)) {
        std::cout << "Picked up a playable card! Press RETURN to play it: ";
        std::cin.ignore(100, '\n');
        players[0]->playCard(players[0]->getHandSize()-1, discardPile);
        this->playedEight(players[0]);
      }
    }
  }
}

/**
* Checks if the draw pile is empty, and reinitializes should it be empty.
*/
void CrazyEights::pileEmpty() {
  if (drawPile->getSize() == 0) {
    std::cout << "Draw Pile is empty, refilling..." << std::endl;
    Card* topDiscard = discardPile->pop();
    while (discardPile->getSize() != 0) {
      drawPile->insertCard(discardPile->pop());
    }
    drawPile->shuffle();
    discardPile->insertCard(topDiscard);
  }
}

/**
* Prompts a player for an index of a card in thier hand to play
* @param correctPile The CardPile to check for validity of a play
*/
int CrazyEights::promptForPlay(CardPile* correctPile) {
  bool madeVP = false;
  int index;
  do {
    do {
      std::cout << "Please enter the index of the card you want to play: "
      << std::endl;
      for (int i = 0; i < players[0]->getHandSize(); i++) {
        std::cout << "\t" << i << ". ";
        std::cout << *(players[0]->getHand()->getCards()[i]);
        std::cout << std::endl;
      }
      std::cout << "Index: ";
      std::cin >> index;
    } while (index < 0 || index >= players[0]->getHandSize());

    madeVP = players[0]->validPlay(players[0]->getHand()->getCards()[index],
    correctPile);
    if (!madeVP) {
      std::cout << "Playing that card is not valid. Try again." << std::endl;
    }
  } while (!madeVP);
  std::cout << std::endl << "Playing card: "
            << *(players[0]->getHand()->getCards()[index])
  << std::endl;
  return index;
}

/**
* Checks to see if a player's lst play was an eight, and prompts them for a
* new Suit if true
* @param p The player to be prompted, if true.
*/
void CrazyEights::playedEight(CrazyEightsPlayer* p) {
  if (discardPile->top()->getRank() == 8) {
    std::cout << "Played a Crazy Eight! Asking for Suit..." << std::endl;
    if (p->isComputer) {
      currentSuit = Suit::spades;
    } else {
      char choice;
      bool madeChoice = false;
      do {
        std::cout << "Enter the Suit you want to switch to (H,D,S, or C): ";
        std::cin >> choice;
        std::cin.ignore(100, '\n');
        switch (choice) {
          case 'h':
          case 'H': currentSuit = Suit::hearts; madeChoice = true; break;
          case 'd':
          case 'D': currentSuit = Suit::diamonds; madeChoice = true; break;
          case 's':
          case 'S': currentSuit = Suit::spades; madeChoice = true; break;
          case 'c':
          case 'C': currentSuit = Suit::clubs; madeChoice = true; break;
          default: std::cout << "Invalid choice. Try again."
                             << std::endl; break;
        }
      } while (!madeChoice);
    }
    std::cout << "\t Suit was changed to " << currentSuit << "!" << std::endl;
  }
}
