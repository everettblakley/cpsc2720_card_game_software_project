/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "Hand.h"
#include <vector>

/**
* Destructor for a Hand object, deallocates the vector of Card pointers
*/
Hand::~Hand() {
  for (int i = 0; i < cards.size(); i++) {
    delete cards[i];
    cards[i] = nullptr;
  }
}

/**
* Allows output of a Hand to the screen
*/
std::ostream& operator<<(std::ostream& os, Hand& h) {
  std::vector<Card*> hCards = h.getCards();
  for (int i = 0; i < hCards.size(); i++) {
    os << *hCards[i] << " ";
  }
  return os;
}

/**
 * Removes Card at index i
 * @param i index of the card to be removed
 * @throw illegal_card_container_error if \a i is outside bounds
 */
Card* Hand::removeCard(int i) {
  if (i < 0 || i >= cards.size()) {
    throw illegal_card_container_error("Index is out of bounds");
  } else {
    Card* out = cards.at(i);
    cards.erase(cards.begin() + i);
    return out;
  }
}

/**
 * Finds the index of a Card in a Hand
 * @param inCard a pointer to the Card in question
 * @return the index if the it is in the hands, -1 if it is not
 */
int Hand::find(Card* inCard) {
  for (int i = 0; i < cards.size(); i++) {
    if (inCard == cards[i]) {
      return i;
    }
  }
  return -1;
}
