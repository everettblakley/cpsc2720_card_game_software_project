/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "Player.h"

/**
* Constructor for a Player
* @param comp Boolean variable to determine if the Player is a computer
*/
Player::Player(const bool comp) : isComputer(comp) {
  hand = new Hand();
}

/**
* Destructor for a Player
*/
Player::~Player() {
  delete hand;
  hand = nullptr;
}

/**
* Draws a card from the top of the CardPile
* @param pile The CardPile to be drawn from
*/
void Player::drawCard(CardPile* pile) {
  hand->insertCard(pile->pop());
}

/**
* @return The Player objects Hand
*/
Hand* Player::getHand() {
  return hand;
}

/**
* @return The size of the Hand container
*/
int Player::getHandSize() {
  return hand->getSize();
}
