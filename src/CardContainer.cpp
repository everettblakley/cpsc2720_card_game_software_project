/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CardContainer.h"
#include "Exceptions.h"
#include <vector>

/**
* @return The size of the CardContainer
*/
int CardContainer::getSize() {
  return cards.size();
}

/**
* Inserts a Card into a CardContainer, with an option for a specific index
* @param inCard The card to be inserted
* @param i The index to be inserted at
* @throw illegal_card_container_error Thrown if the index is out of bounds
*/
void CardContainer::insertCard(Card* inCard, int i) {
  if (i == -1) {
    cards.push_back(inCard);
  } else {
      if (i > cards.size() || i < 0) {
        throw illegal_card_container_error("Placement index out of bounds");
    } else {
        cards.insert(cards.begin()+i, inCard);
      }
    }
}

/**
* "Pop" a Card off the top of the deck
* @return The Card that was removed
* @throw illegal_card_container_error Thrown if the CardContainer is empty
*/
Card* CardContainer::pop() {
  if (cards.size() == 0) {
    throw illegal_card_container_error("Cannot pop an empty CardContainer");
  } else {
    Card* outCard = cards[cards.size() - 1];
    cards.pop_back();
    return outCard;
  }
}

/**
* @return The vector of Card pointers
*/
std::vector<Card*> CardContainer::getCards() {
  return cards;
}
