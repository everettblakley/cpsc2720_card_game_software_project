/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "Suit.h"
#include "Exceptions.h"
#include <iostream>


// Overloaded << operator, to allow output of the suit
std::ostream& operator<< (std::ostream& os, const Suit& suit) {
  // Check which suit it is and output the corresponding char
  switch (suit) {
    case Suit::clubs: os << 'C'; break;
    case Suit::diamonds: os << 'D'; break;
    case Suit::hearts: os << 'H'; break;
    case Suit::spades: os << 'S'; break;
  }
  return os;
}
