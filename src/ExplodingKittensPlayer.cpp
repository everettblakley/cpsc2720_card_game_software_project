/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/
#include "ExplodingKittensPlayer.h"

/**
* Destructor for a ExplodingKittensPlayer
*/
ExplodingKittensPlayer::~ExplodingKittensPlayer() {
  delete hand;
  hand = nullptr;
}

/**
* Check to see if a certain Card is a valid play
* @param card The Card to be examined
* @param pile The CardPile to be compared to (top Card)
* @return True if the play is valid, false otherwise
*/
bool ExplodingKittensPlayer::validPlay(const Card* card, CardPile* pile) {
  if (card->getKind() == Kind::standard_card
  || card->getKind() == Kind::cat
  || card->getKind() == Kind::defuse
  || card->getKind() == Kind::exploding_kitten) {
    return false;
  } else if (card->getKind() == Kind::nope) {
    if (pile->getSize() == 0) {
      return false;
    } else if (pile->top()->getKind() == Kind::defuse
      || pile->top()->getKind() == Kind::cat) {
        return false;
    } else {
      return true;
    }
  } else {
    return true;
  }
}

/**
* Draws a card from a CardPile
* @param pile The CardPile to draw from
*/
void ExplodingKittensPlayer::drawCard(CardPile* pile) {
  Player::drawCard(pile);
  if (!isComputer) {
    std::cout << "You picked up a ";
    std::cout << *(hand->getCards()[this->getHandSize()-1]) << std::endl;
  }
}
