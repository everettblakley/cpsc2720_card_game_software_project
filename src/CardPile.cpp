/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "CardPile.h"
#include "Exceptions.h"
#include <vector>

/**
* Destructor for a CardPile, deallocates the vector of Card pointers
*/
CardPile::~CardPile() {
  for (int i = 0; i < cards.size(); i++) {
    delete cards[i];
    cards[i] = nullptr;
  }
}

/**
* Shuffle a CardPile randomly
*/
void CardPile::shuffle() {
  std::vector<Card*> tempVec;

  while (this->getCards().size() > 0) {
    std::srand(time(0));
    unsigned int index = std::rand() % cards.size();
    tempVec.push_back(cards[index]);
    cards.erase(cards.begin()+index);
  }
  cards = tempVec;
}

/**
* @return the maximum size of the CardPile
*/
int CardPile::getMaxSize() {
  return maxSize;
}

/**
* Insert a Card into the CardPile, either at a specified point or at the top
* @param inCard The Card to be inserted
* @param i The index to place the Card at, defaulted to -1 if the Card is to be
* inserted at the back.
*/
void CardPile::insertCard(Card* inCard, int i) {
  if (cards.size() < maxSize) {
    CardContainer::insertCard(inCard, i);
  } else {
    throw
    illegal_card_container_error("Cannot add more than the max # of cards");
  }
}

/**
* @return The top card of the CardPile, but does not remove
*/
Card* CardPile::top() {
  if (cards.size() > 0) {
    return cards[cards.size()-1];
  } else {
    throw illegal_card_container_error("Cannot return top of empty CardPile");
  }
}
