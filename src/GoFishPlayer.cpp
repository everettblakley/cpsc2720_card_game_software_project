/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "GoFishPlayer.h"
#include <vector>

/**
* Destructor for a GoFishPlayer
*/
GoFishPlayer::~GoFishPlayer() {
  for (int i = 0; i < books.size(); i++) {
    delete books[i];
    books[i] = nullptr;
  }
}

/**
* "Fish" a opposing Player for a Card
* @param opp The opponent to fish from
* @param inCard The Card that will be compared to
* @return True if the Card is fished successfully, false otherwise
*/
bool GoFishPlayer::fish(GoFishPlayer* opp, Card* inCard) {
  std::vector<Card*> oppCards = opp->getHand()->getCards();
  for (int i = 0; i < oppCards.size(); i++) {
    if (inCard->getRank() == oppCards[i]->getRank() && inCard != oppCards[i]) {
      Card* theirCard = opp->getHand()->removeCard(i);
      Card* yourCard = hand->removeCard(hand->find(inCard));
      books.push_back(theirCard);
      books.push_back(yourCard);
      if (this->isComputer) {
        std::cout << "\t Computer made a book." << std::endl;
      } else {
        std::cout << "You made a book." << std::endl;
      }
      return true;
    }
  }
  return false;
}

/**
* Determine the score of the GoFishPlayer by number of books
* @return The score of the GoFishPlayer
*/
int GoFishPlayer::getScore() {
  return books.size()/2;
}

/**
* Draws a Card from a pile and checks to see if a book was made
* @param pile The CardPile to draw from.
*/
void GoFishPlayer::drawCard(CardPile* pile) {
  Player::drawCard(pile);
  if (!isComputer) {
    std::cout << "You picked up a ";
    std::cout << *(hand->getCards()[this->getHandSize()-1]) << std::endl;
  }
  this->fish(this, hand->getCards()[this->getHandSize()-1]);
}

/**
* @return A pointer to the players books (vector of Cards)
*/
std::vector<Card*>* GoFishPlayer::getBooks() {
  return &books;
}

/**
* Clears a players books. Deallocates and decreases size
*/
void GoFishPlayer::clearBooks() {
  for (unsigned int i = 0; i < books.size(); i++) {
    delete books[i];
    books[i] = nullptr;
  }
    books.clear();
}
