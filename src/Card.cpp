/*
* CPSC 2720 Fall 2018
* Copyright 2018 Team Gandalf
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#include "Card.h"
#include "Kind.h"
#include "Suit.h"
#include "Exceptions.h"
#include <string>

/**
* Constructor for an ExplodingKittens Card
* @param k The Kind of the Card
*/
Card::Card(const Kind& k)
    : kind(k), suit(Suit::exploding_kitten), rank(1) {}

/**
* @param s The Suit of the Card
* @param r The integer Rank of the Card
*/
Card::Card(const Suit& s, const int& r)
  : kind(Kind::standard_card), suit(s) {
    if (r < 1 || r > 13) {
      throw invalid_rank_error("Rank must be between 1 and 13");
    } else {
      rank = r;
    }
}

/**
* @return The Kind of the Card
*/
Kind Card::getKind() const {
  return kind;
}
/**
* @return The rank of the Card as an integer
*/
unsigned int Card::getRank() const {
  return rank;
}

/**
* @return The Suit of the Card
*/
Suit Card::getSuit() const {
  return suit;
}

/**
* Tests two Card objects for the same Kind
* @param otherCard The other Card to be examined
*/
bool Card::sameKind(const Card& otherCard) {
  return kind == otherCard.getKind();
}

/**
* Tests two Card objects for the same rank
* @param otherCard The other Card to be examined
*/
bool Card::sameRank(const Card& otherCard) {
  return rank == otherCard.getRank();
}

/**
* Tests two Card objects for the same Suit
* @param otherCard The other Card to be examined
*/
bool Card::sameSuit(const Card& otherCard) {
  return suit == otherCard.getSuit();
}

/**
* Overloaded operator, Tests if two Card objects share all attributes
* @param otherCard The other Card to be examined
*/
bool Card::operator==(const Card & otherCard) {
  if (this->sameKind(otherCard)
    && this->sameRank(otherCard)
    && this->sameSuit(otherCard))
    return true;
  return false;
}

/**
* Overloaded ostream output operator
* @return The ostream represetntation of the Card
*/
std::ostream& operator<<(std::ostream& os, const Card& c) {
  if (c.getKind() == Kind::standard_card) {
    std::string r;
    switch (c.getRank()) {
      case 1: r = 'A'; break;
      case 11: r = 'J'; break;
      case 12: r = 'Q'; break;
      case 13: r = 'K'; break;
      default: r = std::to_string(c.getRank()); break;
    }
    os << r << c.getSuit();
  } else {
    os << c.getKind();
  }
  return os;
}
