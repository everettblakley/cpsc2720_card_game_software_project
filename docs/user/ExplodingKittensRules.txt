********************************************************************************
*                           Rules for Exploding Kittens                        *
********************************************************************************
- Exploding Kittens, as described by the box, is a "highly-strategic,
  kitty-powered version of Russian Roulette." Players take turns playing cards
  in an effort to be the last one standing.

- A players turn consists of them playing as many valid cards as they like from
  their hand as they want, then ending the turn by drawing a card from the draw
  pile.

- Hidden inside the deck is an Exploding Kitten. When drawn, the player must
  immediately show this card and either defuse it with a Defuse card, or die.

- Players can use an assortment of special cards to move, mitigate or avoid an
  Exploding Kitten. They are:

  ~ Attack: (!-! ATTENTION: DIFFERENT FROM THE REGULAR CARD GAME !-!)
            End your turn, and force the next player to draw a card. Not the
            same as skip. Afterwards, they play their turn as normal.
  ~ Defuse: Can only be used to defuse an Exploding Kitten. Can be stolen.
  ~ Favor: Another player must give you a Card of their choice from their hand
  ~ Nope: Stop the action of another player. Defuses and cats cannot be noped.
          You may nope a nope, and nope a nope that noped a nope.
  ~ See The Future: Privately view the top 3 cards of the draw pile.
  ~ Shuffle: Shuffle the draw pile
  ~ Skip: End your turn without drawing a card.
  ~ Cat (assorted): Does nothing of any value. They're pretty cute though.

- When an Exploding Kitten is defused, the player who defused it may put it
  anywhere in the draw pile, but cannot shuffle the deck.

- The last player left standing WINS!
********************************************************************************
