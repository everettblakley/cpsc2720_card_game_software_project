~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ USER MANUAL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Welcome to Team Gandalf's Software Development Project! This user manual will
hopefully help clean up some questions you have about our CardGame program.

To Start, please select a game you would like to play using the prompts on
screen. Enter the correct number of your game to begin!

From here, each game with give you the option to:
    * Start a new game.
    * Load a previously saved game.
    * Display the rules for the game.
    * Quit the game and return to the games menu.

As before, simply navigate the menus by entering the numeric value for the
option you would like.

Each game takes place against a simple computer controlled opponent, so just
imagine you are a Harvard Student playing a 1st Grader in chess. You will
notice the computer does not make very complicated plays. This is normal!

When beginning a game, each game will initialize and set up itself, no work
needs to be done by you at all! From here, you will begin your turn first.

If you don't understand the game, feel free to read the rules to brush up
using the rules option. From here, simply follow the prompts on screen and
input the correct value for the option you wish to select.
(Note: the menus inside a game use characters instead of integers)

You can save a game at any point by using the save option. This will save all
your progress up to that point, so you can pick up at any point!

There is NO AUTOSAVE feature, so you must save before quitting if you want to
keep your progress!


Thank you for playing our games!
 ~ Team Gandalf (Everett Blakley & Logan Wilkie)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
