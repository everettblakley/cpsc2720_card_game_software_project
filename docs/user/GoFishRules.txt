********************************************************************************
*                              Rules for Go Fish!:                             *
********************************************************************************
- Each player is dealt 7 cards to begin the game. If they run out, they are
  dealt 7 more cards (or however many are left in the draw pile). 

- Players take turns "fishing" (asking each other) for cards of specific numeric
  values from their hand. 

  ~ If the opposing player has a card with the same numeric value, or "rank", 
    the opposing player gives the card to the "fisher" (The player asking). The
    fisher then continue asking for more cards unless...

  ~ If the opposing player does not have a card with the same rank, they say
    "Go Fish!" and the fisher draws a card from the draw pile. The players then
    switch turns

- When a pair of cards with the same rank are in a players hand, they become a
  "book". Each book becomes a point to the player.

- The game ends when all cards are made into books, where none are left in any 
  player hands or the draw pile. The player with the most points or books WINS!

********************************************************************************
